<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use Illuminate\Support\Facades\Route;


Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');

    Route::get('user', 'UserController@show');

    // Common
    Route::group(['prefix' => 'common', 'namespace' => 'Common'], function () {
        Route::get('skpd/index', 'SkpdController@index');
        Route::get('unit-skpd/index', 'UnitSkpdController@index');
        Route::get('journal-mode/index', 'JournalModeController@index');
        Route::get('transaction-type/index', 'TransactionTypeController@index');
        Route::get('account/index', 'AccountController@index');
        Route::resource('journal-type', 'JournalTypeController')->only(['index', 'show']);
    });

    // Kebijakan Akuntansi
    Route::group(['prefix' => 'kebijakan-akuntansi', 'namespace' => 'KebijakanAkuntansi'], function () {
        Route::get('transaksi-penerimaan/index', 'TransaksiPenerimaanController@index');
        Route::resource('setting-kebijakan', 'KebijakanController');
    });

    // Kebijakan Akuntansi
    Route::group(['prefix' => 'kebijakan-akuntansi', 'namespace' => 'KebijakanAkuntansi'], function () {
        Route::get('transaksi-penerimaan/index', 'TransaksiPenerimaanController@index');
    });

    // Jurnal Pendapatan
    Route::group(['prefix' => 'jurnal-pendapatan', 'namespace' => 'JurnalPendapatan'], function () {
        Route::get('transaksi', 'TransaksiController@index');
        Route::get('transaksi/{transaction}', 'TransaksiController@show');
        Route::post('transaksi/export', 'ExportController@exportTransaksi');
        Route::get('buku-jurnal', 'BukuJurnalController@index');
    });

    Route::group(['prefix' => 'jurnal', 'namespace' => 'Jurnal'], function () {
        Route::get('jurnalmanual/export', 'JurnalManualController@export');
        Route::get('jurnalmanual/downloadberkas/{jurnalpenyesuaian}', 'JurnalManualController@downloadBerkas');
        Route::resource('jurnalmanual', 'JurnalManualController');
    });

    Route::get('print/cetaklra', 'PrintController@cetakLra');
    Route::get('print/cetakoperasional', 'PrintController@cetakOperasional');
    Route::get('print/cetakneraca', 'PrintController@cetakNeraca');
    Route::get('print/cetakjurnalumum', 'PrintController@cetakJurnalumum');
});

Route::group(['middleware' => 'guest:api'], function () {
    
    Route::post('login', 'LoginController@login');


    // Route::post('register', 'Auth\RegisterController@register');

    // Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    // Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    // Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    // Route::post('email/resend', 'Auth\VerificationController@resend');

    // Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    // Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});
