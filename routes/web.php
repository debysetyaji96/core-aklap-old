<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

// Route::get('{path}', function () {
//     return file_get_contents(public_path('_nuxt/index.html'));
// })->where('path', '(.*)');

if (!App::environment('production')) {
    Route::get('export/coa', 'AccountExportController@export');
}

Route::get('print/test', 'PrintController@test');
Route::get('print/perubahan-saldo-anggaran-lebih', 'PrintController@printPerubahanSaldoAnggaranLebih');
