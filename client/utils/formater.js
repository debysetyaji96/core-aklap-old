export const getChipColor = (status) => {
  let chipColor = null
  if (status === 'PUBLISHED') {
    chipColor = 'success'
  } else if (status === 'APPROVED') {
    chipColor = 'success'
  }
  return chipColor
}

export const formattedJournalModes = (jounalModes) => {
  if (!jounalModes || ! jounalModes.length) return

  const modeCode = jounalModes.map(m => m.code)
  const strCode = modeCode.join(',')

  return strCode
}
