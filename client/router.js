import Vue from 'vue'
import Router from 'vue-router'
import { scrollBehavior } from '~/utils'

Vue.use(Router)

const page = path => () => import(`~/pages/${path}`).then(m => m.default || m)

const routes = [
  { path: '/', name: 'welcome', component: page('welcome.vue') },


  //new menu
  { path: '/jurnalUmum', name: 'jurnal-umum', component: page('jurnalUmum/index.vue') },
  { path: '/kebijakan-akuntansi', name: 'kebijakan-akuntansi', component: page('accountingPolicy/index.vue') },
  { path: '/kewajiban', name: 'kewajiban', component: page('kewajiban/index.vue') },
  { path: '/asset', name: 'asset', component: page('asset/index.vue') },
  { path: '/jurnal/pendapatan', name: 'lra-pendapatan', component: page('journal/revenue.vue') },
  { path: '/jurnal/manual', name: 'jurnal-manual', component: page('journal/revenue.vue') },
  { path: '/jurnalmanual', component: page('jurnalManual/index.vue'), children: [
  { path: ':params?', name: 'jurnalmanual.list', component: page('jurnalManual/_params.vue') },
  ], meta: { label: 'Jurnal Manual' } },

  { path: '/jurnallra', component: page('jurnalLra/index.vue'), meta: { label: 'Jurnal Lra' } },
  
  { path: '/lra/pendapatan-btl', name: 'lra-pendapatan-btl', component: page('LRA/pendapatan-btl.vue') },
  { path: '/laporanrealisasianggaran', name: 'laporanlra', component: page('laporanKeuangan/lra/index.vue') },
  { path: '/laporanoperasional', name: 'laporanoperasional', component: page('laporanKeuangan/operasional/index.vue') },
  { path: '/laporanneraca', name: 'laporanneraca', component: page('laporanKeuangan/neraca/index.vue') },
  { path: '/laporanjurnalumum', name: 'laporanjurnalumum', component: page('laporanKeuangan/jurnalumum/index.vue') },
  
  { path: '/login', name: 'login', component: page('auth/login.vue') },
  { path: '/register', name: 'register', component: page('auth/register.vue') },
  { path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue') },
  { path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue') },
  { path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue') },
  { path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue') },

  { path: '/home', name: 'home', component: page('home.vue') },
  { path: '/settings',
    component: page('settings/index.vue'),
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: page('settings/profile.vue') },
      { path: 'password', name: 'settings.password', component: page('settings/password.vue') }
    ] }
]

export function createRouter () {
  return new Router({
    routes,
    scrollBehavior,
    mode: 'history',
    base: process.env.baseRoute || "/",
  })
}
