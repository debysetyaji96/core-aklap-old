<!DOCTYPE html>
<html>
<head>
    <title>Test</title>
    <style>
        .titimangsa {
            float: right;
            width: 250px;
        }
        .titimangsa2 {
            float: left;
            width: 250px;
        }
        .fontregular {
            font-size: 12px;
        }
        thead {
            display: table-header-group
        }
        tfoot {
            display: table-row-group
        }
        tr {
            page-break-inside: avoid
        }
        .cr {
            width: 200px;
            padding: 10px;
            position: absolute;
            text-align: center;
            color: #f0f0f0;
        }
        .cr-sticky {
            position: fixed;
        }
        .cr-top { 
            top: 25px;
        }
        .cr-left {
            left: -50px;
        }
        .cr-top.cr-left,
        .cr-bottom.cr-right {
            -webkit-transform: rotate(-45deg);
        }
        .cr-red {
            background-color: #e43;
        }
    </style>
</head>
<body>
    <table style="width: 100%" align="center" border="0">
        <tr>
            <td align="center">
                <font face="Segoe UI" size="5"><b>PEMERINTAH PROVINSI JAWA BARAT</b><br></font></h2>
                <font face="Segoe UI" size="5"><b>BUKU PEMBANTU KAS<br></b></font></h1>
            </td>
        </tr>
    </table>
    <font face="Segoe UI" size="5">{{ $data }}</font>
</body>
</html>