@php
  if (!isset($depth)) {
    $depth = 0;
  }
@endphp
@foreach ($tableRows as $rowData)
  @php
    $hasChildren = property_exists($rowData, "children")
      && is_array($rowData->children)
      && !empty($rowData->children);
  @endphp
  @if ($hasChildren)
    <tr>
      <td>{{ $rowData->name }}</td>
      <td></td>
      <td></td>
    </tr>
    @include('laporan.perubahan-saldo-anggaran-lebih-row', [
      'tableRows' => $rowData->children,
      'depth' => $depth + 1,
    ])
    <tr>
      <td>
        <b style="padding-left: {{ ($depth + 2) * 1.5 }}rem;" >Subtotal</b>
      </td>
      <td>{{ $rowData->subtotalBefore }}</td>
      <td>{{ $rowData->subtotalAfter }}</td>
    </tr>
  @else
    <tr>
      <td>
        <span style="padding-left: {{ $depth * 1.5 }}rem;" >{{ $rowData->name }}</span>
      </td>
      <td>{{ $rowData->before }}</td>
      <td>{{ $rowData->after }}</td>
    </tr>
  @endif
@endforeach