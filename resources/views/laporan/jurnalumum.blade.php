<table cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th colspan=7 style="text-align: center; vertical-align: center;"><b>PEMERINTAH {{$namaProvinsi}}</b></th>
    </tr>
    <tr>
        <th colspan=7 style="text-align: center; vertical-align: center;"><b>JURNAL UMUM</b></th>
    </tr>
    <tr>
    <th colspan=7 style="text-align: center; vertical-align: center;"><b>{{$periode}}</b></th>
    </tr>
    <tr>
    </tr>
    @if($namaSkpd!="")
    <tr>
        <th colspan=6 style="vertical-align: center;"><b>SKPD : {{ $namaSkpd }}</b></th>
    </tr>
    @endif
    <tr>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2 style="height:40px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>TANGGAL</b></td>
            <td rowspan=2 style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>NO.BM</b></td>
            <td rowspan=2 style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>KODE REKENING</b></td>
            <td rowspan=2 style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>URAIAN</b></td>
            <td rowspan=2 style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>REF</b></td>
            <td colspan=2 style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>JUMLAH (Rp)</b></td>
        </tr>
        <tr>
            <td style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>DEBET</b></td>
            <td style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>KREDIT</b></td>
        </tr>
        <tr>
            <td style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>1</b></td>
            <td style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>2</b></td>
            <td style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>3</b></td>
            <td style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>4</b></td>
            <td style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>5</b></td>
            <td style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>6</b></td>
            <td style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>7</b></td>
        </tr>
        <tr><td style="border: 1px solid #000000;width:15px;"></td><td style="border: 1px solid #000000;width:28px;"></td><td style="border: 1px solid #000000;width:20px;"></td><td style="border: 1px solid #000000;width:40px;"></td><td style="border: 1px solid #000000;width:15px;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>

    @php $total_debet = 0; $total_kredit = 0; @endphp    
    @foreach($jurnalumum as $recp)
        <tr>
            <td style="text-align: center; border: 1px solid #000000;">{{ $recp->tanggal }}</td>
            <td style="text-align: center; border: 1px solid #000000;">{{ $recp->nobm }}</td>
            <td style="text-align: center; border: 1px solid #000000;">{{ $recp->kode_akun }}</td>
            <td style="border: 1px solid #000000;">{{ $recp->nama_akun }}</td>
            <td style="border: 1px solid #000000;">{{ $recp->ref }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ $recp->debet > 0 ? number_format($recp->debet) : "" }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ $recp->kredit > 0 ? number_format($recp->kredit) : "" }}</td>
         </tr>
              
        @php $total_debet += $recp->debet; $total_kredit += $recp->kredit;  
        @endphp
    @endforeach
    <tr>
        <td colspan=5 style="border: 1px solid #000000; text-align: center;"><b>Jumlah</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($total_debet) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($total_kredit) }}</b></td>
    </tr>    
    

    
    
    </tbody>
</table>
