<table cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th colspan=6 style="text-align: center; vertical-align: center;"><b>PEMERINTAH {{$namaProvinsi}}</b></th>
    </tr>
    @if($namaSkpd!="")
    <tr>
        <th colspan=6 style="text-align: center; vertical-align: center;"><b>SKPD {{ $namaSkpd }}</b></th>
    </tr>
    @endif
    <tr>
        <th colspan=6 style="text-align: center; vertical-align: center;"><b>NERACA</b></th>
    </tr>
    <tr>
    <th colspan=6 style="text-align: center; vertical-align: center;"><b>{{$periode}}</b></th>
    </tr>
    <tr>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan=4 style="height:40px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>Uraian</b></td>
            <td style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>2021</b></td>
            <td style="width:20px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>2020</b></td>
        </tr>
        <tr><td style="border: 1px solid #000000;width:2px;"></td><td style="border: 1px solid #000000;width:2px;"></td><td style="border: 1px solid #000000;width:2px;"></td><td style="border: 1px solid #000000;width:40px;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
            
    
    @php $no = 1; $prevstruk=null; $prevkel=null; $prevnamakel=null; $prevjenis=null; $prevoby=null; $prevroby=null; $nama_struk=null; $realisasi1_struk=null; $realisasi2_struk=null; $realisasi1_kel=null; $realisasi2_kel=null; @endphp
    @foreach($aset as $recp)
        @if($prevstruk != $recp->kode_struk)
        @php $nama_struk=$recp->nama_struk; $realisasi1_struk=$recp->realisasi1_struk; $realisasi2_struk=$recp->realisasi2_struk; @endphp
        <tr>
            <td colspan=4 style="border: 1px solid #000000;"><b>{{ $recp->nama_struk }}</b></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>    
        </tr>
        @endif
        @if($prevkel != $recp->kode_kel)
        
        @if(!is_null($prevnamakel))
        <tr>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td colspan=3 style="border: 1px solid #000000; text-align: center;"><b>{{ "Jumlah ".ucfirst(strtolower($prevnamakel)) }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi1_kel) }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi2_kel) }}</b></td>
        </tr>    
        @endif
        <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
        <tr>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td colspan=3 style="border: 1px solid #000000;"><b>{{ $recp->nama_kel }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b></b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b></b></td>
        </tr>    
        @endif

        @if($prevjenis != $recp->kode_jenis && $recp->kode_struk!="3")
        <tr>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td colspan=2 style="border: 1px solid #000000;">{{ $recp->nama_jenis }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi1_jenis) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi2_jenis) }}</td>
         </tr>
        @endif
        @if($prevoby != $recp->kode_obyek && $recp->kode_jenis=="1.1.01")
        <tr>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td style="border: 1px solid #000000;">{{ $recp->nama_obyek }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi1) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi2) }}</td>
         </tr>
        @endif
              
        @php $prevstruk=$recp->kode_struk; $prevkel=$recp->kode_kel; $prevnamakel=$recp->nama_kel; $realisasi1_kel=$recp->realisasi1_kel; $realisasi2_kel=$recp->realisasi2_kel; $prevjenis=$recp->kode_jenis; $prevoby=$recp->kode_obyek;  
        @endphp
    @endforeach
    @if(!is_null($prevnamakel))
    <tr>
        <td style="border: 1px solid #000000;"><b></b></td>
        <td colspan=3 style="border: 1px solid #000000; text-align: center;"><b>{{ "Jumlah ".ucfirst(strtolower($recp->nama_kel)) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi1_kel) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi2_kel) }}</b></td>
    </tr>    
    @endif
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
    @if(!is_null($nama_struk))
    <tr>
        <td colspan=4 style="border: 1px solid #000000;text-align: center;"><b>{{ "JUMLAH ".strtoupper($recp->nama_struk) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi1_struk) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi2_struk) }}</b></td>
     </tr>
    @endif
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>





    @php $no = 1; $prevstruk=null; $prevkel=null; $prevnamakel=null; $prevjenis=null; $prevoby=null; $prevroby=null; $nama_struk=null; $realisasi1_struk=null; $realisasi2_struk=null; $realisasi1_kel=null; $realisasi2_kel=null; @endphp
    @foreach($kewajiban as $recp)
        @if($prevstruk != $recp->kode_struk)
        @php $nama_struk=$recp->nama_struk; $realisasi1_struk=$recp->realisasi1_struk; $realisasi2_struk=$recp->realisasi2_struk; @endphp
        <tr>
            <td colspan=4 style="border: 1px solid #000000;"><b>{{ $recp->nama_struk }}</b></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>    
        </tr>
        @endif
        @if($prevkel != $recp->kode_kel)
        
        @if(!is_null($prevnamakel))
        <tr>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td colspan=3 style="border: 1px solid #000000; text-align: center;"><b>{{ "Jumlah ".ucfirst(strtolower($prevnamakel)) }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi1_kel) }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi2_kel) }}</b></td>
        </tr>    
        @endif
        <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
        <tr>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td colspan=3 style="border: 1px solid #000000;"><b>{{ $recp->nama_kel }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b></b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b></b></td>
        </tr>    
        @endif

        @if($prevjenis != $recp->kode_jenis && $recp->kode_struk!="3")
        <tr>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td colspan=2 style="border: 1px solid #000000;">{{ $recp->nama_jenis }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi1_jenis) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi2_jenis) }}</td>
         </tr>
        @endif
        @if($prevoby != $recp->kode_obyek && $recp->kode_jenis=="1.1.01")
        <tr>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td style="border: 1px solid #000000;">{{ $recp->nama_obyek }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi1) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi2) }}</td>
         </tr>
        @endif
              
        @php $prevstruk=$recp->kode_struk; $prevkel=$recp->kode_kel; $prevnamakel=$recp->nama_kel; $realisasi1_kel=$recp->realisasi1_kel; $realisasi2_kel=$recp->realisasi2_kel; $prevjenis=$recp->kode_jenis; $prevoby=$recp->kode_obyek;  
        @endphp
    @endforeach
    @if(!is_null($prevnamakel))
    <tr>
        <td style="border: 1px solid #000000;"><b></b></td>
        <td colspan=3 style="border: 1px solid #000000; text-align: center;"><b>{{ "Jumlah ".ucfirst(strtolower($recp->nama_kel)) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi1_kel) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi2_kel) }}</b></td>
    </tr>    
    @endif
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
    @if(!is_null($nama_struk))
    <tr>
        <td colspan=4 style="border: 1px solid #000000;text-align: center;"><b>{{ "JUMLAH ".strtoupper($recp->nama_struk) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi1_struk) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi2_struk) }}</b></td>
     </tr>
    @endif
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>






    @php $no = 1; $prevstruk=null; $prevkel=null; $prevnamakel=null; $prevjenis=null; $prevoby=null; $prevroby=null; $nama_struk=null; $realisasi1_struk=null; $realisasi2_struk=null; $realisasi1_kel=null; $realisasi2_kel=null; @endphp
    @foreach($ekuitas as $recp)
        @if($prevstruk != $recp->kode_struk)
        @php $nama_struk=$recp->nama_struk; $realisasi1_struk=$recp->realisasi1_struk; $realisasi2_struk=$recp->realisasi2_struk; @endphp
        <tr>
            <td colspan=4 style="border: 1px solid #000000;"><b>{{ $recp->nama_struk }}</b></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>    
        </tr>
        @endif
        @if($prevkel != $recp->kode_kel)
        <tr>
            <td style="border: 1px solid #000000;"><b></b></td>
            <td colspan=3 style="border: 1px solid #000000;"><b>{{ $recp->nama_kel }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($recp->realisasi1_kel) }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($recp->realisasi2_kel) }}</b></td>
        </tr>    
        @endif

        @php $prevstruk=$recp->kode_struk; $prevkel=$recp->kode_kel; $prevnamakel=$recp->nama_kel; $realisasi1_kel=$recp->realisasi1_kel; $realisasi2_kel=$recp->realisasi2_kel; $prevjenis=$recp->kode_jenis; $prevoby=$recp->kode_obyek;  
        @endphp
    @endforeach
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
    @if(!is_null($nama_struk))
    <tr>
        <td colspan=4 style="border: 1px solid #000000;text-align: center;"><b>{{ "JUMLAH ".strtoupper($recp->nama_struk) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi1_struk) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi2_struk) }}</b></td>
     </tr>
    @endif
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>


    
    
    </tbody>
</table>
