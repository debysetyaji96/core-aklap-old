<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="{{ public_path('build/stylesheets/laporan/common.css') }}">
  </head>
  <body>
    <main class="laporan">
      <h1 class="laporan__title text-center">
        {{ $governmentUnitName }}
        <br>
        LAPORAN PERUBAHAN SALDO ANGGARAN LEBIH
        <br>
        PER 31 DESEMBER {{ $firstYear }} dan {{ $secondYear }}
      </h1>
    </main>
    <p class="text-right" style="margin: 0;">
      (Dalam Rupiah)
    </p>
    <table class="laporan__table-main">
      <thead>
        <tr>
          <th>Uraian</th>
          <th>{{ $firstYear }}</th>
          <th>{{ $secondYear }}</th>
        </tr>
      </thead>
      <tbody>
        @include('laporan.perubahan-saldo-anggaran-lebih-row', [
          'tableRows' => $tableRows,
        ])
      </tbody>
    </table>
  </body>
</html>