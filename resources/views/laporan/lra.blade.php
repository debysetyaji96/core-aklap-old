<table cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th colspan=6 style="text-align: center; vertical-align: center;"><b>PEMERINTAH {{$namaProvinsi}}</b></th>
    </tr>
    <tr>
        <th colspan=6 style="text-align: center; vertical-align: center;"><b>LAPORAN REALISASI ANGGARAN PENDAPATAN DAN BELANJA DAERAH</b></th>
    </tr>
    <tr>
        <th colspan=6 style="text-align: center; vertical-align: center;"><b>{{$periode}}</b></th>
    </tr>
    <tr>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td style="height:40px;width:17px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>Kode Rekening</b></td>
            <td style="width:50px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>URAIAN</b></td>
            <td style="width:25px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>ANGGARAN</b></td>
            <td style="width:25px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>RAEALISASI S.D 31 JANUARI</b></td>
            <td style="width:25px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>LEBIH / (KURANG)</b></td>
            <td style="width:12px;text-align: center; vertical-align: center; border: 1px solid #000000;"><b>%</b></td>
        </tr>
        <tr>
            <td style="text-align: center; vertical-align: center; border: 1px solid #000000;font-size:10px;"><b>1</b></td>
            <td style="text-align: center; vertical-align: center; border: 1px solid #000000;font-size:10px;"><b>2</b></td>
            <td style="text-align: center; vertical-align: center; border: 1px solid #000000;font-size:10px;"><b>3</b></td>
            <td style="text-align: center; vertical-align: center; border: 1px solid #000000;font-size:10px;"><b>4</b></td>
            <td style="text-align: center; vertical-align: center; border: 1px solid #000000;font-size:10px;"><b>5 = 4 - 3</b></td>
            <td style="text-align: center; vertical-align: center; border: 1px solid #000000;font-size:10px;"><b>6 = 4 / 3 *100</b></td>
        </tr>
        <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
            
    
    @php $no = 1; $prevstruk=null; $prevkel=null; $prevjenis=null; $prevoby=null; $prevroby=null; $nama_struk=null; $anggaran_struk=null; $realisasi_struk=null; @endphp
    @foreach($pendapatan as $recp)
        @if($prevstruk != $recp->kode_struk)
        @php $nama_struk=$recp->nama_struk; $anggaran_struk=$recp->anggaran_struk; $realisasi_struk=$recp->realisasi_struk; @endphp
        <tr>
            <td style="border: 1px solid #000000;"><b>{{ $recp->kode_struk }}</b></td>
            <td style="border: 1px solid #000000;"><b>{{ $recp->nama_struk }}</b></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>    
            <td style="border: 1px solid #000000;"></td>    
        </tr>
        @endif
        @if($prevkel != $recp->kode_kel)
        @if($prevstruk == $recp->kode_struk)
        <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
        @endif
        <tr>
            <td style="border: 1px solid #000000;"><b>{{ $recp->kode_kel }}</b></td>
            <td style="border: 1px solid #000000;"><b>{{ $recp->nama_kel }}</b></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>
        </tr>    
        @endif
        @if($prevjenis != $recp->kode_jenis)
        <tr>
            <td style="border: 1px solid #000000;">{{ $recp->kode_jenis }}</td>
            <td style="border: 1px solid #000000;">{{ $recp->nama_jenis }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->anggaran_jenis) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi_jenis) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($recp->realisasi_jenis - $recp->anggaran_jenis)) }}</td>    
            <td style="text-align: center; border: 1px solid #000000;">{{ round(($recp->realisasi_jenis/$recp->anggaran_jenis)*100,2) }}</td>    
         </tr>
        @endif
        @if($prevoby != $recp->kode_obyek && $recp->kode_obyek=="4.2.01.02")
        <tr>
            <td style="border: 1px solid #000000;">{{ $recp->kode_obyek }}</td>
            <td style="border: 1px solid #000000;">{{ $recp->nama_obyek }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->anggaran_obyek) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi_obyek) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($recp->realisasi_obyek - $recp->anggaran_obyek)) }}</td>
            <td style="text-align: center; border: 1px solid #000000;">{{ round(($recp->realisasi_obyek/$recp->anggaran_obyek)*100,2) }}</td>    
         </tr>
        @endif
        @if($prevroby != $recp->kode_robyek && $recp->kode_obyek=="4.2.01.01")
        <tr>
            <td style="border: 1px solid #000000;">{{ $recp->kode_robyek }}</td>
            <td style="border: 1px solid #000000;">{{ $recp->nama_robyek }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->anggaran_robyek) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi_robyek) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($recp->realisasi_robyek - $recp->anggaran_robyek)) }}</td>
            <td style="text-align: center; border: 1px solid #000000;">{{ round(($recp->realisasi_robyek/$recp->anggaran_robyek)*100,2) }}</td>    
         </tr>
        @endif
    
  
        @php $prevstruk=$recp->kode_struk; $prevkel=$recp->kode_kel; $prevjenis=$recp->kode_jenis; $prevoby=$recp->kode_obyek; $prevroby=$recp->kode_robyek; 
        @endphp
    @endforeach
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
    @if(!is_null($nama_struk))
    <tr>
        <td style="border: 1px solid #000000;"></td>
        <td style="border: 1px solid #000000;"><b>{{ "JUMLAH ".strtoupper($nama_struk) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($anggaran_struk) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi_struk) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($realisasi_struk - $anggaran_struk)) }}</b></td>
        <td style="text-align: center; border: 1px solid #000000;"><b>{{ round(($realisasi_struk/$anggaran_struk)*100,2) }}</b></td>    
        </tr>
    @endif
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
    @php $no = 1; $prevstruk=null; $prevkel=null; $prevjenis=null; $prevoby=null; $prevroby=null;  $nama_struk_belanja=null; $anggaran_struk_belanja=null; $realisasi_struk_belanja=null; @endphp
    @foreach($belanja as $recp)
        @if($prevstruk != $recp->kode_struk)
        @php $nama_struk_belanja=$recp->nama_struk; $anggaran_struk_belanja=$recp->anggaran_struk; $realisasi_struk_belanja=$recp->realisasi_struk; @endphp
        <tr>
            <td style="border: 1px solid #000000;"><b>{{ $recp->kode_struk }}</b></td>
            <td style="border: 1px solid #000000;"><b>{{ $recp->nama_struk }}</b></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>    
        </tr>
        @endif
        @if($prevkel != $recp->kode_kel)
        @if($prevstruk == $recp->kode_struk)
        <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
        @endif
        <tr>
            <td style="border: 1px solid #000000;"><b>{{ $recp->kode_kel }}</b></td>
            <td style="border: 1px solid #000000;"><b>{{ $recp->nama_kel }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($recp->anggaran_kel) }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($recp->realisasi_kel) }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($recp->realisasi_kel - $recp->anggaran_kel)) }}</b></td>
            <td style="text-align: center; border: 1px solid #000000;"><b>{{ round(($recp->realisasi_kel/$recp->anggaran_kel)*100,2) }}</b></td>    
        </tr>    
        @endif
        @if($prevjenis != $recp->kode_jenis)
        <tr>
            <td style="border: 1px solid #000000;">{{ $recp->kode_jenis }}</td>
            <td style="border: 1px solid #000000;">{{ $recp->nama_jenis }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->anggaran_jenis) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi_jenis) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($recp->realisasi_jenis - $recp->anggaran_jenis)) }}</td>
            <td style="text-align: center; border: 1px solid #000000;">{{ round(($recp->realisasi_jenis/$recp->anggaran_jenis)*100,2) }}</td>    
         </tr>
        @endif
        
  
        @php $prevstruk=$recp->kode_struk; $prevkel=$recp->kode_kel; $prevjenis=$recp->kode_jenis; 
        @endphp
    @endforeach
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
    @if(!is_null($nama_struk_belanja))
    <tr>
        <td style="border: 1px solid #000000;"></td>
        <td style="border: 1px solid #000000;"><b>{{ "JUMLAH ".strtoupper($nama_struk_belanja) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($anggaran_struk_belanja) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi_struk_belanja) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($realisasi_struk_belanja - $anggaran_struk_belanja)) }}</b></td>
        <td style="text-align: center; border: 1px solid #000000;"><b>{{ round(($realisasi_struk_belanja/$anggaran_struk_belanja)*100,2) }}</b></td>    
        </tr>
    @endif
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
    <tr>
        <td style="border: 1px solid #000000;"></td>
        <td style="text-align: center; vertical-align: center;border: 1px solid #000000;"><b>SURPLUS / (DEFISIT) - LRA</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($anggaran_struk - $anggaran_struk_belanja)) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($realisasi_struk - $realisasi_struk_belanja)) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format(($realisasi_struk - $realisasi_struk_belanja) - ($anggaran_struk - $anggaran_struk_belanja))) }}</b></td>
        <td style="text-align: center; border: 1px solid #000000;"><b>{{ round((($realisasi_struk - $realisasi_struk_belanja)/($anggaran_struk - $anggaran_struk_belanja))*100,2) }}</b></td>    
    </tr>
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>

    @php $no = 1; $prevstruk=null; $prevkel=null; $prevjenis=null; $prevoby=null; $prevroby=null; $nama_struk_pembiayaan_terima=null; $anggaran_struk_pembiayaan_terima=null; $realisasi_struk_pembiayaan_terima=null; $nama_struk_pembiayaan_keluar=null; $anggaran_struk_pembiayaan_keluar=null; $realisasi_struk_pembiayaan_keluar=null; @endphp
    @foreach($pembiayaan as $recp)
        @if($prevstruk != $recp->kode_struk)
        <tr>
            <td style="border: 1px solid #000000;"><b>{{ $recp->kode_struk }}</b></td>
            <td style="border: 1px solid #000000;"><b>{{ $recp->nama_struk }}</b></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>    
            <td style="border: 1px solid #000000;"></td>    
        </tr>

        @endif
        @if($prevkel != $recp->kode_kel)
        @if($prevkel == "6.1")
        <tr>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"><b>{{ "JUMLAH ".strtoupper($nama_struk_pembiayaan_terima) }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($anggaran_struk_pembiayaan_terima) }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi_struk_pembiayaan_terima) }}</b></td>
            <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($realisasi_struk_pembiayaan_terima - $anggaran_struk_pembiayaan_terima)) }}</b></td>
            <td style="text-align: center; border: 1px solid #000000;"><b>{{ round(($realisasi_struk_pembiayaan_terima/$anggaran_struk_pembiayaan_terima)*100,2) }}</b></td>    
         </tr>
        @endif
        @if($prevstruk == $recp->kode_struk)
        <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
        @endif
        <tr>
            <td style="border: 1px solid #000000;"><b>{{ $recp->kode_kel }}</b></td>
            <td style="border: 1px solid #000000;"><b>{{ $recp->nama_kel }}</b></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>
            <td style="border: 1px solid #000000;"></td>
        </tr>    
        @endif
        
        @if($prevoby != $recp->kode_obyek)
        <tr>
            <td style="border: 1px solid #000000;">{{ $recp->kode_obyek }}</td>
            <td style="border: 1px solid #000000;">{{ $recp->nama_obyek }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->anggaran_obyek) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ number_format($recp->realisasi_obyek) }}</td>
            <td style="text-align: right; border: 1px solid #000000;">{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($recp->realisasi_obyek - $recp->anggaran_obyek)) }}</td>
            <td style="text-align: center; border: 1px solid #000000;">{{ round(($recp->realisasi_obyek/$recp->anggaran_obyek)*100,2) }}</td>    
         </tr>
        @endif
        
    
        @if($recp->kode_kel == "6.1")
            @php $nama_struk_pembiayaan_terima=$recp->nama_kel; $anggaran_struk_pembiayaan_terima=$recp->anggaran_kel; $realisasi_struk_pembiayaan_terima=$recp->realisasi_kel; @endphp
        @else
            @php $nama_struk_pembiayaan_keluar=$recp->nama_kel; $anggaran_struk_pembiayaan_keluar=$recp->anggaran_kel; $realisasi_struk_pembiayaan_keluar=$recp->realisasi_kel; @endphp
        @endif
        @php $prevstruk=$recp->kode_struk; $prevkel=$recp->kode_kel; $prevjenis=$recp->kode_jenis; $prevoby=$recp->kode_obyek; 
        @endphp
    @endforeach
    @if($prevkel == "6.2")
        <tr>
        <td style="border: 1px solid #000000;"></td>
        <td style="border: 1px solid #000000;"><b>{{ "JUMLAH ".strtoupper($nama_struk_pembiayaan_keluar) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($anggaran_struk_pembiayaan_keluar) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ number_format($realisasi_struk_pembiayaan_keluar) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($realisasi_struk_pembiayaan_keluar - $anggaran_struk_pembiayaan_keluar)) }}</b></td>
        <td style="text-align: center; border: 1px solid #000000;"><b>{{ round(($realisasi_struk_pembiayaan_keluar/$anggaran_struk_pembiayaan_keluar)*100,2) }}</b></td>    
        </tr>
    @endif
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
    <tr>
        <td style="border: 1px solid #000000;"></td>
        <td style="border: 1px solid #000000;"><b>Pembiayaan Netto</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($anggaran_struk_pembiayaan_terima - $anggaran_struk_pembiayaan_keluar)) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($realisasi_struk_pembiayaan_terima - $realisasi_struk_pembiayaan_keluar)) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format(($realisasi_struk_pembiayaan_terima - $realisasi_struk_pembiayaan_keluar) - ($anggaran_struk_pembiayaan_terima - $anggaran_struk_pembiayaan_keluar))) }}</b></td>
        <td style="text-align: center; border: 1px solid #000000;"><b>{{ ($anggaran_struk_pembiayaan_terima - $anggaran_struk_pembiayaan_keluar)<=0 ? 0 : round((($realisasi_struk_pembiayaan_terima - $realisasi_struk_pembiayaan_keluar)/($anggaran_struk_pembiayaan_terima - $anggaran_struk_pembiayaan_keluar))*100,2) }}</b></td>    
    </tr>
    <tr><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td><td style="border: 1px solid #000000;"></td></tr>
    <tr>
        <td style="border: 1px solid #000000;"></td>
        <td style="border: 1px solid #000000;"><b>Sisa Lebih Pembiayaan Anggaran (SILPA)</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format(($anggaran_struk - $anggaran_struk_belanja) + ($anggaran_struk_pembiayaan_terima - $anggaran_struk_pembiayaan_keluar))) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format(($realisasi_struk - $realisasi_struk_belanja) + ($realisasi_struk_pembiayaan_terima - $realisasi_struk_pembiayaan_keluar))) }}</b></td>
        <td style="text-align: right; border: 1px solid #000000;"><b>{{ preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format((($realisasi_struk - $realisasi_struk_belanja) + ($realisasi_struk_pembiayaan_terima - $realisasi_struk_pembiayaan_keluar)) - (($anggaran_struk - $anggaran_struk_belanja) + ($anggaran_struk_pembiayaan_terima - $anggaran_struk_pembiayaan_keluar)))) }}</b></td>
        <td style="text-align: center; border: 1px solid #000000;"><b>{{ (($anggaran_struk - $anggaran_struk_belanja) - ($anggaran_struk_pembiayaan_terima - $anggaran_struk_pembiayaan_keluar))<=0 ? 0 : round(((($realisasi_struk - $realisasi_struk_belanja) + ($realisasi_struk_pembiayaan_terima - $realisasi_struk_pembiayaan_keluar)) / (($anggaran_struk - $anggaran_struk_belanja) - ($anggaran_struk_pembiayaan_terima - $anggaran_struk_pembiayaan_keluar)))*100,2) }}</b></td>    
    </tr>
    
    </tbody>
</table>
