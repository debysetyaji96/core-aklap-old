<?php

namespace App\Exports;

use App\Models\Transaction;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Shared\StringHelper;

class TransaksiPendapatanExport implements
    FromQuery,
    Responsable,
    WithHeadings,
    ShouldAutoSize,
    WithMapping,
    WithColumnFormatting,
    WithEvents
{
    use Exportable;

    public $startDate;

    public $endDate;

    public $order;

    public $orderDirection;

    public $search;

    function __construct(array $params = [])
    {

        $this->startDate = isset($params['start_date']) ? $params['start_date'] : null;
        $this->endDate = isset($params['end_date']) ? $params['end_date'] : null;
        $this->order = isset($params['order']) ? $params['order'] : null;
        $this->orderDirection = isset($params['order_direction']) ? $params['order_direction'] : 'desc';
        $this->search = isset($params['search']) ? $params['search'] : null;
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    public function query()
    {
        $transactionQuery = Transaction::query()->whereHas('transactionType')
            ->whereHas('journalMode')
            ->whereHas('jenisBukti', function ($jenisBukti) {
                $jenisBukti->whereIn('code', ['tbp', 'skp', 'skr']);
            })
            // ->whereHas('journals') // TODO: Must be specified
            ->join('jenis_bukti', 'transactions.jenis_bukti_id', 'jenis_bukti.id')
            ->join('journal_modes', 'transactions.journal_mode_id', 'journal_modes.id')
            ->select([
                DB::raw('ROW_NUMBER() OVER() AS Row'),
                DB::raw("document_number::varchar"),
                'transaction_date',
                DB::raw("jenis_bukti.name as jenis_bukti_name"),
                DB::raw("case when length(description) > 0 then left(description, 50) || '...' else '' end as description"),
                DB::raw("round(amount::double precision) as amount"),
                DB::raw("journal_modes.code as journal_mode_code"),
            ]);

        if ($this->startDate) {
            $transactionQuery->where('transaction_date', '>=', $this->startDate);
        }

        if ($this->endDate) {
            $transactionQuery->where('transaction_date', '<=', $this->endDate);
        }

        // Order
        switch ($this->order) {
            case 'document_number':
                $transactionQuery->orderBy('document_number', $this->orderDirection);
                break;
            case 'amount':
                $transactionQuery->orderBy('amount', $this->orderDirection);
                break;
            default:
                $transactionQuery->orderBy('transaction_date', $this->orderDirection);
                break;
        }

        // Search by keyword
        if ($this->search) {
            $transactionQuery->where(function ($query) {
                $query->where('document_number', 'ilike', '%' . $this->search . '%')
                    ->orWhere('description', 'ilike', '%' . $this->search . '%');
            });
        }

        return $transactionQuery;
    }

    function headings(): array
    {
        return [
            'No.',
            'Nomor Dokumen',
            'Tanggal Dokumen',
            'Jenis Bukti',
            'Deskripsi',
            'Nominal',
            'Tipe Jurnal'
        ];
    }

    function map($transaction): array
    {
        return [
            $transaction->getAttribute('row'),
            $transaction->getAttribute('document_number'),
            $transaction->getAttribute('transaction_date'), // Date::stringToExcel(),
            $transaction->getAttribute('jenis_bukti_name'),
            $transaction->getAttribute('description'),
            $transaction->getAttribute('amount'),
            $transaction->getAttribute('journal_mode_code'),
        ];
    }

    function columnFormats(): array
    {
        return [
            // 'D' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC
        ];
    }

    function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event){
                $event->sheet->getStyle('A1:G1')->applyFromArray([
                    'font'=> [
                        'bold'=> true
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => '84BD86',
                        ],
                    ],
                ]);

                $event->sheet->getStyle('G')->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ]);
            } 
        ];   
    }
}
