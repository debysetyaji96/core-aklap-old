<?php

namespace App\Exports;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class JurnalumumExport extends DefaultValueBinder implements FromView, WithCustomValueBinder, WithColumnFormatting
{
    use Exportable;

    public $params;

    function __construct(array $params = [])
    {
        $this->params = $params;
    }

    function columnFormats(): array
    {
        return [
          //  'A' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING2
        ];
    }

    public function bindValue(Cell $cell, $value)
    {
        if ($cell->getColumn() == 'A') {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function view(): View
    {
        return view('laporan.jurnalumum', $this->params);
    }
}
