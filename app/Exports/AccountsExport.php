<?php

namespace App\Exports;

use App\Models\Account;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class AccountsExport implements FromCollection, Responsable
{
    use Exportable;

    private $fileName = 'chart-of-accounts.xlsx';
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Account::all();
    }
}
