<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JurnalPenyesuaianResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id, 
            'nomor_bukti'=> $this->nomor_bukti, 
            'id_daerah'=> $this->id_daerah,
            'id_skpd'=> $this->id_skpd,
            'nama_kegiatan'=> $this->nama_kegiatan,
            'nama_subkegiatan'=> $this->nama_subkegiatan,
            'tanggal'=> $this->tanggal,
            'keterangan'=> $this->keterangan,
            'berkas'=> $this->berkas,
            'debet'=> $this->debet,
            'kredit'=> $this->kredit,
            'skpd'=> $this->skpd,
            'detailJurnal'=> $this->detailJurnal
        ];
    }
}
