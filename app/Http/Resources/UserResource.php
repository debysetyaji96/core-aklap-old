<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'idUser'=> $this->idUser, 
            'userName'=> $this->userName, 
            'firstName'=> $this->firstName,
            'idPegawai'=> $this->idPegawai,
            'idSkpd'=> $this->idSkpd,
            'idDaerah'=> $this->idDaerah,
            'idRole'=> $this->idRole,
            'pegawai'=> $this->pegawai,
            'daerah'=> $this->daerah,
        ];
    }
}
