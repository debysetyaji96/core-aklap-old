<?php

namespace App\Http\Controllers\Jurnal;

use App\Http\Controllers\Controller;
use App\Models\JurnalPenyesuaian;
use App\Models\JurnalPenyesuaianDetail;
use Illuminate\Http\Request;
use App\Models\Account;
use App\Exports\AjaxTableExport;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\JurnalPenyesuaianResource;
use App\Rules\uniqueCode;
use DB;
use Excel;

class JurnalManualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $returnModel = false)
    {
        $user = $request->user();
        $models = JurnalPenyesuaian::leftJoin(DB::raw("(select sum(debet) as debet, sum(kredit) as kredit, jurnal_penyesuaian_id from jurnal_penyesuaian_detail group by jurnal_penyesuaian_id) as jurnal_penyesuaian_detail"),'jurnal_penyesuaian_detail.jurnal_penyesuaian_id','=','jurnal_penyesuaian.id');
        $searchparams = json_decode($request->searchparams, true);

        /*if (in_array($user->pegawai ? $user->pegawai->idJabatan : 0, [10, 3, 13, 1])){
            $models->where('id_skpd',$user->idSkpd);
        }
        if ($user->daerah && !$user->daerah->is_pusat) {
            $models->where('id_daerah',$user->idDaerah);
        }*/
        if ($searchparams && is_array($searchparams)) {
            foreach ($searchparams as $key => $value) {
                if (empty($value) && $value !== 0) continue;
                switch ($key) {
                    case 'nomor_bukti':
                        $models->where('nomor_bukti','ilike','%'.$value.'%');
                        break;
                    case 'idSkpd':
                        if (isset($searchparams['idSkpdUnit']) && !empty($searchparams['idSkpdUnit'])) {
                            if ($searchparams['idSkpdUnit'] == 'all') {
                                $idSkpds = RSkpd::where('id_daerah', $user->idDaerah)->where('is_locked', 0)->where('id_unit', $value)->pluck('id_skpd');
                                $models->whereIn('id_skpd',$idSkpds);
                            } else {
                                $models->where('id_skpd',$searchparams['idSkpdUnit']);
                            }
                        } else {
                            if ($value != 'all') {
                                $models->where('id_skpd',$value);
                            }
                        }
                        break;
                    case 'tanggalFrom':
                        $models->where('tanggal','>=',$value);
                        break;
                    case 'tanggalTo':
                        $models->where('tanggal','<=',$value);
                        break;
                }
            }
        }
        $total = floatval($models->sum('debet'));
        $count = $models->count();
        $page = 1;
        $sorted = false;
        if ($searchparams && is_array($searchparams)) {
            foreach ($searchparams as $key => $value) {
                switch ($key) {
                    case 'page':
                        $page = $value;
                        break;
                    case 'sort':
                        $sorted = true;
                        $sorts = [];
                        foreach($value as $val) {
                            $x = explode('@', $val);
                            $sorts[$x[0]] = $x[1];
                        }
                        foreach ($sorts as $sort => $dir) {
                            if (!in_array($dir, ['asc','desc'])) continue;
                            switch ($sort) {
                                case 'nomor_bukti':
                                    $models = $models->orderBy('nomor_bukti',$dir);
                                    break;
                                case 'tanggal':
                                    $models = $models->orderBy('tanggal',$dir);
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    case 'search':
                        if (empty($value)) break;
                        $models->where(function($q) use ($value) {
                            $q->where('nomor_bukti', 'ilike' , "%$value%")
                                ->orWhere('keterangan', 'ilike' , "%$value%")
                                ->orWhere('nama_kegiatan', 'ilike' , "%$value%");

                        });
                        break;
                }
            }
        }
        if (!$sorted) {
            $models = $models->orderByRaw('nomor_bukti');
        }
        $perpage = $request->get('perpage',50);

        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        
        return $returnModel ? $models : response()->json([
            'success' => true,
            'data' => JurnalPenyesuaianResource::collection($models),
            'count' => $count,
            'total' => $total,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        $id_skpd = null;
        /*if (in_array($user->pegawai ? $user->pegawai->idJabatan : 0, [10, 3, 13, 1])){
            $id_skpd = $user->idSkpd;
        } else if (!empty($request->idSkpdUnit)) {
            $id_skpd = $request->idSkpdUnit;
        } else if (!empty($request->idSkpd)) {
            $id_skpd = $request->idSkpd;
        } else {
            $id_skpd = $user->idSkpd;
        }*/
        $id_skpd = $request->idSkpd;
        $model = new JurnalPenyesuaian;
        $request->validate([
            'tanggal'=> 'required',
            'id_skpd'=> 'id_skpd',
            'debet' => 'required|numeric|min:0|not_in:0|in:'.$request->kredit,
            'kredit' => 'required|numeric|min:0|not_in:0|in:'.$request->debet,
            'nomor_bukti'=> ['required', new uniqueCode($model,[["id_daerah", $user->idDaerah], ["id_skpd", $id_skpd]])],
            'keterangan'=> 'required'
        ]);
        DB::beginTransaction();
        if ($request->fileberkas) {
            $pathFotoBerkas = Storage::disk('public')->putFile(
                'file_berkas', $request->file('fileberkas')
            );
            $model->berkas = $pathFotoBerkas;
        }
        $model->tanggal = $request->tanggal;
        $model->nomor_bukti = $request->nomor_bukti;
        $model->nama_kegiatan = $request->nama_kegiatan;
        $model->nama_subkegiatan = $request->nama_subkegiatan;
        $model->keterangan = $request->keterangan;
        $model->id_skpd = $id_skpd;
        $model->id_daerah = $user->idDaerah;
        if ($model->save()){
            $listDetail = json_decode($request->detailJurnal);
            foreach ($listDetail as $detail){
                $modeldetail = new JurnalPenyesuaianDetail;
                $modeldetail->jurnal_penyesuaian_id = $model->id;
                $modeldetail->account_id = $detail->account_id;
                $modeldetail->debet = $detail->debet;
                $modeldetail->kredit = $detail->kredit;
                $modeldetail->save();
            }
        }
        DB::commit();
        $model = JurnalPenyesuaian::leftJoin(DB::raw("(select sum(debet) as debet, sum(kredit) as kredit, jurnal_penyesuaian_id from jurnal_penyesuaian_detail group by jurnal_penyesuaian_id) as jurnal_penyesuaian_detail"),'jurnal_penyesuaian_detail.jurnal_penyesuaian_id','=','jurnal_penyesuaian.id')->where('jurnal_penyesuaian.id', $model->id)->first();
        return response()->json([
            'success' => true,
            'model' =>  new JurnalPenyesuaianResource($model)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JurnalPenyesuaian  $jurnalpenyesuaian
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, JurnalPenyesuaian $jurnalpenyesuaian)
    {
        $user = $request->user();
        if ($jurnalpenyesuaian->idDaerah != $user->idDaerah) {
            return response()->json([
                'success' => false,
                'message' => "Data not found"
            ], 404);
        }
        return response()->json([
            'success' => true,
            'model' => $jurnalpenyesuaian
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JurnalPenyesuaian  $jurnalpenyesuaian
     * @return \Illuminate\Http\Response
     */
    public function edit(JurnalPenyesuaian $jurnalpenyesuaian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JurnalPenyesuaian  $jurnalpenyesuaian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JurnalPenyesuaian $jurnalmanual)
    {
        $user = $request->user();
        if ($jurnalmanual->id_daerah != $user->idDaerah) {
            return response()->json([
                'success' => false,
                'message' => "Data not found"
            ], 404);
        }
        $id_skpd = null;
        /*if (in_array($user->pegawai ? $user->pegawai->idJabatan : 0, [10, 3, 13, 1])){
            $id_skpd = $user->idSkpd;
        } else if (!empty($request->idSkpdUnit)) {
            $id_skpd = $request->idSkpdUnit;
        } else if (!empty($request->idSkpd)) {
            $id_skpd = $request->idSkpd;
        } else {
            $id_skpd = $user->idSkpd;
        }*/
        $id_skpd = $request->id_skpd;
        $request->validate([
            'tanggal'=> 'required',
            'debet' => 'required|numeric|min:0|not_in:0|in:'.$request->kredit,
            'kredit' => 'required|numeric|min:0|not_in:0|in:'.$request->debet,
            'nomor_bukti'=> ['required', new uniqueCode($jurnalmanual,[["id_daerah", $user->idDaerah], ["id_skpd", $id_skpd]])],
            'keterangan'=> 'required'
        ]);
        
        DB::beginTransaction();
        if ($request->fileberkas) {
            $pathFotoBerkas = Storage::disk('public')->putFile(
                'file_berkas', $request->file('fileberkas')
            );
            $jurnalmanual->berkas = $pathFotoBerkas;
        }
        $jurnalmanual->tanggal = $request->tanggal;
        $jurnalmanual->nomor_bukti = $request->nomor_bukti;
        $jurnalmanual->nama_kegiatan = $request->nama_kegiatan;
        $jurnalmanual->nama_subkegiatan = $request->nama_subkegiatan;
        $jurnalmanual->keterangan = $request->keterangan;
        $jurnalmanual->id_skpd = $id_skpd;
        $jurnalmanual->id_daerah = $user->idDaerah;
        if ($jurnalmanual->save()){
            $listDetail = json_decode($request->detailJurnal);
            $ids = [];
            foreach ($listDetail as $detail){
                $modeldetail = null;
                if ($detail->id!='' && $detail->id!==null){
                    $modeldetail = JurnalPenyesuaianDetail::find($detail->id);
                }
                
                if (is_null($modeldetail)){
                    $modeldetail = new JurnalPenyesuaianDetail;
                    $modeldetail->jurnal_penyesuaian_id = $jurnalmanual->id;
                }
                $modeldetail->account_id = $detail->account_id;
                $modeldetail->debet = $detail->debet;
                $modeldetail->kredit = $detail->kredit;
                $modeldetail->save();
                array_push($ids, $modeldetail->id);
            }
            JurnalPenyesuaianDetail::whereNotIn('id', $ids)->where('jurnal_penyesuaian_id', $jurnalmanual->id)->delete();
        }

        DB::commit();
        $model = JurnalPenyesuaian::leftJoin(DB::raw("(select sum(debet) as debet, sum(kredit) as kredit, jurnal_penyesuaian_id from jurnal_penyesuaian_detail group by jurnal_penyesuaian_id) as jurnal_penyesuaian_detail"),'jurnal_penyesuaian_detail.jurnal_penyesuaian_id','=','jurnal_penyesuaian.id')->where('jurnal_penyesuaian.id', $jurnalmanual->id)->first();
        return response()->json([
            'success' => true,
            'model' => new JurnalPenyesuaianResource($model)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JurnalPenyesuaian  $jurnalpenyesuaian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, JurnalPenyesuaian $jurnalmanual)
    {
        $user = $request->user();
        
        if ($jurnalmanual->id_daerah != $user->idDaerah) {
            return response()->json([
                'success' => false,
                'message' => "Data not found"
            ], 404);
        }
        try {
            JurnalPenyesuaianDetail::where([['jurnal_penyesuaian_id', $jurnalmanual->id]])->delete();
            $jurnalmanual->delete();
            return response()->json([
                'success' => true,
                'message' => "Data berhasil dihapus"
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function downloadBerkas(JurnalPenyesuaian $jurnalpenyesuaian){
        if ($jurnalpenyesuaian->berkas){
            $file = public_path(DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR.$jurnalpenyesuaian->berkas);
            $headers = array('Content-Type: '.mime_content_type($file),);
            return response()->download($file, 'berkas.doc',$headers);
        }
    }

    public function export(Request $request )
    {
        $models = $this->index($request, true);
        foreach ($models as $idx => &$model) {
            $model->no = $idx+1;
        }
        //dd(JurnalPenyesuaian::all());
        //dd(($models)(JurnalPenyesuaian));
        $header = [
            'No',
            'No Dokumen',
            'Tanggal',
            'Kode Rekening',
            'Nama Rekening',
            'Nilai',
            'Keterangan',
            'Kode SKPD',
            'SKPD',
            'Kode Unit',
            'Unit',
        ];
        
        $mapping = function($model) {
            return [
                $model->no,
                $model->nomor_bukti,
                date("d/m/Y", strtotime($model->tanggal)),
                $model->kodeRekening ? $model->kodeRekening : '-',
                $model->namaRekening ? $model->namaRekening : '-',
                $model->nilaiDetailTbp,
                $model->keterangan,
                $model->skpd ? $model->skpd->kode_skpd : "-",
                $model->skpd ? $model->skpd->nama_skpd : "-",
                $model->unit ? $model->unit->kode_skpd : "",
                $model->unit ? $model->unit->nama_skpd : "",
            ];
        };
        $column_format = [
            'A' => NumberFormat::FORMAT_NUMBER,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => '[$-id-ID]dd mmmm yyyy;@',
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => '_("Rp"* #,##0.00_);_("Rp"* \(#,##0.00\);_("Rp"* "-"??_);_(@_)'
        ];
        $style = [
            ['cell' => 'A1:K1',
            'style' => [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => ['argb' => "dedcd6"]
                ]
            ]],
            ['cell' => 'A1:K'.(count($models)+1),
            'style' => [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '000000'],
                    ],
                ],
            ]]
        ];
        $filename = 'Penerimaan v' . date('Y-m-d H:i:s') . '.xlsx';
        return Excel::download(new AjaxTableExport($models, $header, $mapping, $column_format, $style), $filename);
    }
}
