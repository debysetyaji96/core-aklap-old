<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    public function login(Request $request)
    {        
        $request->validate(
            ['username'=> 'required', 'password'=> 'required'], 
            $request->only(['username', 'password'])
        );

        $user = User::query()->where('userName', $request->username)->first();

        if (!$user) {
            return response()->json([
                'success'=> false,
                'message'=> 'Data pengguna tidak ditemukan',
                'data'=> null,
            ], 401);
        }

        $passwordMatch = Hash::check($request->password, $user->getAttribute('passwordUser'));

        if ($passwordMatch) {
            $token = JWTAuth::fromUser($user);
            // Auth::login($user);

            return response()->json([
                'success'=> true,
                'message'=> 'Berhasil login',
                'data'=> [
                    'token'=> [
                        'access_token' => $token,
                        'token_type' => 'bearer',
                        // 'expires_in' => auth()->factory()->getTTL() * 60
                    ], 
                    //  Arr::only($user->toArray(), ['userName', 'firstName', 'idPegawai', 'idSkpd', 'idDaerah'])
                    'user'=> new UserResource($user)
                ],
            ]);
        }

        return response()->json([
            'success'=> false,
            'message'=> 'Unauthenticated',
            'data'=> null,
        ], 401);
    }
}
