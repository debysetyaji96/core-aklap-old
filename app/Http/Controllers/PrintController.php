<?php

namespace App\Http\Controllers;

use PDF;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\LraExport;
use App\Exports\NeracaExport;
use App\Exports\OperasionalExport;
use App\Exports\JurnalumumExport;

class PrintController extends Controller
{
    public function cetakLra (Request $request) {
        $namaProvinsi = "PROVINSI JAWA BARAT";
        $periode = "TAHUN ANGGARAN 2021";
        $searchparams = json_decode($request->searchparams, true);
        $pendapatanCollection = DB::connection('pgsql_simda')
            ->table('penatausahaan.rak_pendapatan as a')
            ->join(DB::raw('(select distinct id_daerah, id_skpd, id_unit, id_akun, kode_akun, nama_akun, tahun from ebudgeting.data_pendapatan) as b'),  
            function ($sql) {
                $sql->on('a.id_daerah', '=', 'b.id_daerah');
                $sql->on('a.id_skpd', '=', 'b.id_skpd');
                $sql->on('a.id_unit', '=', 'b.id_unit');
                $sql->on('a.id_akun', '=', 'b.id_akun');
                $sql->on('a.tahun', '=', 'b.tahun');
            })
            ->leftJoin(DB::raw('(select r.*, t."idDaerah", t."idSkpd", t.tahun from "Penerimaan"."DetailTbp" r inner join "Penerimaan"."Tbp" t on r."idTbp"=t."idTbp" ) r'),  
            function ($sql) {
                $sql->on('a.id_daerah', '=', 'r.idDaerah');
                $sql->on('a.id_skpd', '=', 'r.idSkpd');
                $sql->on('a.id_akun', '=', 'r.idRekening');
                $sql->on(DB::raw("a.tahun||''"), '=', 'r.tahun');
            })
            ->join('ReferensiPenerimaan.RekeningAkun as ro',  function ($sql) {
                $sql->on(DB::raw('substring(b.kode_akun,1,12)'), '=', 'ro.kodeRekening'); $sql->on('ro.level_rekening', '=', DB::raw("5"));
            })
            ->join('ReferensiPenerimaan.RekeningAkun as o',  function ($sql) {
                $sql->on(DB::raw('substring(b.kode_akun,1,9)'), '=', 'o.kodeRekening'); $sql->on('o.level_rekening', '=', DB::raw("4"));
            })
            ->join('ReferensiPenerimaan.RekeningAkun as j',  function ($sql) {
                $sql->on(DB::raw('substring(b.kode_akun,1,6)'), '=', 'j.kodeRekening'); $sql->on('j.level_rekening', '=', DB::raw("3"));
            })
            ->join('ReferensiPenerimaan.RekeningAkun as k',  function ($sql) {
                $sql->on(DB::raw('substring(b.kode_akun,1,3)'), '=', 'k.kodeRekening'); $sql->on('k.level_rekening', '=', DB::raw("2"));
            })
            ->join('ReferensiPenerimaan.RekeningAkun as s',  function ($sql) {
                $sql->on(DB::raw('substring(b.kode_akun,1,1)'), '=', 's.kodeRekening'); $sql->on('s.level_rekening', '=', DB::raw("1"));
            })
            ->select('s.kodeRekening as kode_struk', 's.namaRekening as nama_struk', 'k.kodeRekening as kode_kel', 'k.namaRekening as nama_kel', 
            'j.kodeRekening as kode_jenis', 'j.namaRekening as nama_jenis', 'o.kodeRekening as kode_obyek', 'o.namaRekening as nama_obyek', 'ro.kodeRekening as kode_robyek', 'ro.namaRekening as nama_robyek', 
            DB::raw('sum(bulan_1+bulan_2+bulan_3+bulan_4+bulan_5+bulan_6+bulan_7+bulan_8+bulan_9+bulan_10+bulan_11+bulan_12) OVER (PARTITION BY s."kodeRekening" ORDER BY s."kodeRekening") as anggaran_struk'), DB::raw('sum(coalesce(r."nilaiDetailTbp",0)) OVER (PARTITION BY s."kodeRekening" ORDER BY s."kodeRekening") realisasi_struk'),
            DB::raw('sum(bulan_1+bulan_2+bulan_3+bulan_4+bulan_5+bulan_6+bulan_7+bulan_8+bulan_9+bulan_10+bulan_11+bulan_12) OVER (PARTITION BY k."kodeRekening" ORDER BY k."kodeRekening") as anggaran_kel'), DB::raw('sum(coalesce(r."nilaiDetailTbp",0)) OVER (PARTITION BY k."kodeRekening" ORDER BY k."kodeRekening") realisasi_kel'),
            DB::raw('sum(bulan_1+bulan_2+bulan_3+bulan_4+bulan_5+bulan_6+bulan_7+bulan_8+bulan_9+bulan_10+bulan_11+bulan_12) OVER (PARTITION BY j."kodeRekening" ORDER BY j."kodeRekening") as anggaran_jenis'), DB::raw('sum(coalesce(r."nilaiDetailTbp",0)) OVER (PARTITION BY j."kodeRekening" ORDER BY j."kodeRekening") realisasi_jenis'),
            DB::raw('sum(bulan_1+bulan_2+bulan_3+bulan_4+bulan_5+bulan_6+bulan_7+bulan_8+bulan_9+bulan_10+bulan_11+bulan_12) OVER (PARTITION BY o."kodeRekening" ORDER BY o."kodeRekening") as anggaran_obyek'), DB::raw('sum(coalesce(r."nilaiDetailTbp",0)) OVER (PARTITION BY o."kodeRekening" ORDER BY o."kodeRekening") realisasi_obyek'),
            DB::raw('sum(bulan_1+bulan_2+bulan_3+bulan_4+bulan_5+bulan_6+bulan_7+bulan_8+bulan_9+bulan_10+bulan_11+bulan_12) OVER (PARTITION BY ro."kodeRekening" ORDER BY ro."kodeRekening") as anggaran_robyek'), DB::raw('sum(coalesce(r."nilaiDetailTbp",0)) OVER (PARTITION BY ro."kodeRekening" ORDER BY ro."kodeRekening") realisasi_robyek'))
            ->groupBy('s.kodeRekening', 's.namaRekening', 'k.kodeRekening', 'k.namaRekening', 
            'j.kodeRekening', 'j.namaRekening', 'o.kodeRekening', 'o.namaRekening', 'ro.kodeRekening', 'ro.namaRekening', 'b.kode_akun', 'b.nama_akun', 
            'bulan_1', 'bulan_2', 'bulan_3', 'bulan_4', 'bulan_5', 'bulan_6', 'bulan_7', 'bulan_8', 'bulan_9', 'bulan_10', 'bulan_11', 'bulan_12', DB::raw('coalesce(r."nilaiDetailTbp",0)'))->distinct();
        
         $belanjaCollection = DB::connection('pgsql_simda')
            ->table('Referensi.Rekening as r')
            ->join('Referensi.Rekening3 as r3', DB::raw('substring(r."kodeRekening",1,6)'), '=', DB::raw("split_part(r3.\"kodeRekening3\", '.', 1)||'.'||split_part(r3.\"kodeRekening3\", '.', 2)||'.'||lpad(split_part(r3.\"kodeRekening3\", '.', 3),2,'0')"))
            ->join('Referensi.Rekening2 as r2', DB::raw('substring(r."kodeRekening",1,3)'), '=', "r2.kodeRekening2")
            ->join('Referensi.Rekening1 as r1', DB::raw('substring(r."kodeRekening",1,1)'), '=', "r1.kodeRekening1")
            ->join('Referensi.Belanja as b', 'b.id_akun', '=', 'r.idRekening')

            ->leftJoin(DB::raw("(select dspp.* from \"Simda\".\"DetailSpp\" dspp join \"Simda\".\"Spp\" spp on dspp.\"idSpp\"=spp.\"idSpp\" where spp.\"verifikasiSpp\"='1') dspp"), 'dspp.idBelanja', '=', 'b.idBelanja')
            ->select('r1.kodeRekening1 as kode_struk', 'r1.namaRekening1 as nama_struk', 'r2.kodeRekening2 as kode_kel', 'r2.namaRekening2 as nama_kel', 'r3.kodeRekening3 as kode_jenis', 'r3.namaRekening3 as nama_jenis', 
            DB::raw('sum(b."paguTotalBelanja") OVER (PARTITION BY "r1"."kodeRekening1" ORDER BY "r1"."kodeRekening1") as anggaran_struk'), DB::raw('sum(dspp."nilaiDisetujuiDetailSpp") OVER (PARTITION BY "r1"."kodeRekening1" ORDER BY "r1"."kodeRekening1") realisasi_struk'),
            DB::raw('sum(b."paguTotalBelanja") OVER (PARTITION BY "r2"."kodeRekening2" ORDER BY "r2"."kodeRekening2") as anggaran_kel'), DB::raw('sum(dspp."nilaiDisetujuiDetailSpp") OVER (PARTITION BY "r2"."kodeRekening2" ORDER BY "r2"."kodeRekening2") realisasi_kel'),
            DB::raw('sum(b."paguTotalBelanja") OVER (PARTITION BY "r3"."kodeRekening3" ORDER BY "r3"."kodeRekening3") as anggaran_jenis'), DB::raw('sum(dspp."nilaiDisetujuiDetailSpp") OVER (PARTITION BY "r3"."kodeRekening3" ORDER BY "r3"."kodeRekening3") realisasi_jenis'))
            ->groupBy('r1.kodeRekening1', 'r1.namaRekening1', 'r2.kodeRekening2', 'r2.namaRekening2', 'r3.kodeRekening3', 'r3.namaRekening3', 'r.kodeRekening', 'r.namaRekening',
            'b.paguTotalBelanja', 'dspp.nilaiDisetujuiDetailSpp', 'r1.kodeRekening1', 'r1.namaRekening1', 'r2.kodeRekening2', 'r2.namaRekening2', 'r3.kodeRekening3', 'r3.namaRekening3')->distinct();

        $pembiayaanCollection = DB::connection('pgsql_simda')
            ->table('penatausahaan.rak_pembiayaan as a')
            ->join(DB::raw('(select distinct id_daerah, id_skpd, id_unit, id_akun, kode_akun, nama_akun, tahun from ebudgeting.data_pembiayaan) as b'),  
            function ($sql) {
                $sql->on('a.id_daerah', '=', 'b.id_daerah');
                $sql->on('a.id_skpd', '=', 'b.id_skpd');
                $sql->on('a.id_unit', '=', 'b.id_unit');
                $sql->on('a.id_akun', '=', 'b.id_akun');
                $sql->on('a.tahun', '=', 'b.tahun');
            })
            ->leftJoin(DB::raw('(select r.*, t."idDaerah", t."idSkpd", t.tahun from "Penerimaan"."DetailTbp" r inner join "Penerimaan"."Tbp" t on r."idTbp"=t."idTbp" ) r'),  
            function ($sql) {
                $sql->on('a.id_daerah', '=', 'r.idDaerah');
                $sql->on('a.id_skpd', '=', 'r.idSkpd');
                $sql->on('a.id_akun', '=', 'r.idRekening');
                $sql->on(DB::raw("a.tahun||''"), '=', 'r.tahun');
            })
            ->join('Referensi.Rekening4 as o', DB::raw('substring(b.kode_akun,1,9)'), '=', DB::raw("split_part(o.\"kodeRekening4\", '.', 1)||'.'||split_part(o.\"kodeRekening4\", '.', 2)||'.'||lpad(split_part(o.\"kodeRekening4\", '.', 3),2,'0')||'.'||lpad(split_part(o.\"kodeRekening4\", '.', 4),2,'0')"))
            ->join('Referensi.Rekening3 as j', DB::raw('substring(b.kode_akun,1,6)'), '=', DB::raw("split_part(j.\"kodeRekening3\", '.', 1)||'.'||split_part(j.\"kodeRekening3\", '.', 2)||'.'||lpad(split_part(j.\"kodeRekening3\", '.', 3),2,'0')"))
            ->join('Referensi.Rekening2 as k', DB::raw('substring(b.kode_akun,1,3)'), '=', "k.kodeRekening2")
            ->join('Referensi.Rekening1 as s', DB::raw('substring(b.kode_akun,1,1)'), '=', "s.kodeRekening1")

            ->select('s.kodeRekening1 as kode_struk', 's.namaRekening1 as nama_struk', 'k.kodeRekening2 as kode_kel', 'k.namaRekening2 as nama_kel', 
            'j.kodeRekening3 as kode_jenis', 'j.namaRekening3 as nama_jenis', 'o.kodeRekening4 as kode_obyek', 'o.namaRekening4 as nama_obyek', 
            DB::raw('sum(bulan_1+bulan_2+bulan_3+bulan_4+bulan_5+bulan_6+bulan_7+bulan_8+bulan_9+bulan_10+bulan_11+bulan_12) OVER (PARTITION BY s."kodeRekening1" ORDER BY s."kodeRekening1") as anggaran_struk'), DB::raw('sum(coalesce(r."nilaiDetailTbp",0)) OVER (PARTITION BY s."kodeRekening1" ORDER BY s."kodeRekening1") realisasi_struk'),
            DB::raw('sum(bulan_1+bulan_2+bulan_3+bulan_4+bulan_5+bulan_6+bulan_7+bulan_8+bulan_9+bulan_10+bulan_11+bulan_12) OVER (PARTITION BY k."kodeRekening2" ORDER BY k."kodeRekening2") as anggaran_kel'), DB::raw('sum(coalesce(r."nilaiDetailTbp",0)) OVER (PARTITION BY k."kodeRekening2" ORDER BY k."kodeRekening2") realisasi_kel'),
            DB::raw('sum(bulan_1+bulan_2+bulan_3+bulan_4+bulan_5+bulan_6+bulan_7+bulan_8+bulan_9+bulan_10+bulan_11+bulan_12) OVER (PARTITION BY j."kodeRekening3" ORDER BY j."kodeRekening3") as anggaran_jenis'), DB::raw('sum(coalesce(r."nilaiDetailTbp",0)) OVER (PARTITION BY j."kodeRekening3" ORDER BY j."kodeRekening3") realisasi_jenis'),
            DB::raw('sum(bulan_1+bulan_2+bulan_3+bulan_4+bulan_5+bulan_6+bulan_7+bulan_8+bulan_9+bulan_10+bulan_11+bulan_12) OVER (PARTITION BY o."kodeRekening4" ORDER BY o."kodeRekening4") as anggaran_obyek'), DB::raw('sum(coalesce(r."nilaiDetailTbp",0)) OVER (PARTITION BY o."kodeRekening4" ORDER BY o."kodeRekening4") realisasi_obyek'))
            ->groupBy('s.kodeRekening1', 's.namaRekening1', 'k.kodeRekening2', 'k.namaRekening2', 
            'j.kodeRekening3', 'j.namaRekening3', 'o.kodeRekening4', 'o.namaRekening4', 
            'bulan_1', 'bulan_2', 'bulan_3', 'bulan_4', 'bulan_5', 'bulan_6', 'bulan_7', 'bulan_8', 'bulan_9', 'bulan_10', 'bulan_11', 'bulan_12', DB::raw('coalesce(r."nilaiDetailTbp",0)'))->distinct();
            //->where('is_skpd', 0)
            //->where('is_locked', 0);

        /*if ($search) {
            $unitSkpdCollection->where(function ($query) use ($search) {
                $query->where('kode_unit', 'ilike', '%' . $search . '%')
                    ->orWhere('kode_skpd', 'ilike', '%' . $search . '%')
                    ->orWhere('nama_skpd', 'ilike', '%' . $search . '%');
            });
        }*/
        $pendapatanCollection = $pendapatanCollection->get();
        $belanjaCollection = $belanjaCollection->get();
        $pembiayaanCollection = $pembiayaanCollection->get();


        $namaFile = "LRA_".date("Y_m_d_H_i");
        if ($searchparams['formatFile']=="xls")
            return (new LraExport(["pendapatan" => $pendapatanCollection, "belanja" => $belanjaCollection, "pembiayaan" => $pembiayaanCollection, "namaProvinsi" => $namaProvinsi, "periode" => $periode]))->download($namaFile.'.xlsx');
        else if ($searchparams['formatFile']=="pdf")
            return PDF::loadView('laporan.lra', ["pendapatan" => $pendapatanCollection, "belanja" => $belanjaCollection, "pembiayaan" => $pembiayaanCollection, "namaProvinsi" => $namaProvinsi, "periode" => $periode])
                ->setOptions([
                    'dpi'              => 70,
                    'page-size'        => 'A4',
                    'margin-top'       => 15,
                    'margin-right'     => 15,
                    'margin-left'      => 15,
                    'margin-bottom'    => 15,
                    //'orientation'      => $orientation,
                    //'footer-right'     => ($enabledFooter) ? 'Halaman [page] dari [toPage]' : '',
                    'footer-font-size' => 6,
                    'footer-font-name' => 'Times New Roman',
                ])
                ->stream($namaFile.'.pdf');

    }

    function arrayToTable($arr){
        $arrrow = [];
        foreach($arr as $data){
            //dd($data);
            $arrfield = [];
            foreach(array_keys($data) as $key){
                array_push($arrfield, (gettype($data[$key])=="string" ? "'".str_replace("'", "''", $data[$key])."'" : $data[$key])." as $key");
            }
            array_push($arrrow,"select ".implode(", ", $arrfield));
        }
        return implode(" union all ", $arrrow);
    }

    public function cetakOperasional (Request $request) {
        $namaProvinsi = "PROVINSI JAWA BARAT";
        $periode = "TAHUN ANGGARAN 2021";
        $namaSkpd = "";
        $searchparams = json_decode($request->searchparams, true);
        
        $pendapatanCollection = DB::connection('pgsql_simda')
            ->table('penatausahaan.rak_pendapatan as a')
            ->join(DB::raw('(select distinct id_daerah, id_skpd, id_unit, id_akun, kode_akun, nama_akun, tahun from ebudgeting.data_pendapatan) as b'),  
            function ($sql) {
                $sql->on('a.id_daerah', '=', 'b.id_daerah');
                $sql->on('a.id_skpd', '=', 'b.id_skpd');
                $sql->on('a.id_unit', '=', 'b.id_unit');
                $sql->on('a.id_akun', '=', 'b.id_akun');
                $sql->on('a.tahun', '=', 'b.tahun');
            })
            ->leftJoin(DB::raw('(select r.*, t."idDaerah", t."idSkpd", t.tahun from "Penerimaan"."DetailTbp" r inner join "Penerimaan"."Tbp" t on r."idTbp"=t."idTbp" ) r'),  
            function ($sql) {
                $sql->on('a.id_daerah', '=', 'r.idDaerah');
                $sql->on('a.id_skpd', '=', 'r.idSkpd');
                $sql->on('a.id_akun', '=', 'r.idRekening');
                $sql->on(DB::raw("a.tahun||''"), '=', 'r.tahun');
            })
            ->join('ReferensiPenerimaan.RekeningAkun as j',  function ($sql) {
                $sql->on(DB::raw('substring(b.kode_akun,1,6)'), '=', 'j.kodeRekening'); $sql->on('j.level_rekening', '=', DB::raw("3")); 
            })
            ->join('ReferensiPenerimaan.RekeningAkun as k',  function ($sql) {
                $sql->on(DB::raw('substring(b.kode_akun,1,3)'), '=', 'k.kodeRekening'); $sql->on('k.level_rekening', '=', DB::raw("2"));
            })
            ->join('ReferensiPenerimaan.RekeningAkun as s',  function ($sql) {
                $sql->on(DB::raw('substring(b.kode_akun,1,1)'), '=', 's.kodeRekening'); $sql->on('s.level_rekening', '=', DB::raw("1"));
            })
            ->select('s.kodeRekening as kode_struk', 's.namaRekening as nama_struk', 'k.kodeRekening as kode_kel', 'k.namaRekening as nama_kel', 
            'j.kodeRekening as kode_jenis', 'j.namaRekening as nama_jenis', 
            DB::raw('sum(bulan_1+bulan_2+bulan_3+bulan_4+bulan_5+bulan_6+bulan_7+bulan_8+bulan_9+bulan_10+bulan_11+bulan_12) OVER (PARTITION BY s."kodeRekening" ORDER BY s."kodeRekening") as realisasi1_struk'), DB::raw('sum(coalesce(r."nilaiDetailTbp",0)) OVER (PARTITION BY s."kodeRekening" ORDER BY s."kodeRekening") realisasi2_struk'),
            DB::raw('sum(bulan_1+bulan_2+bulan_3+bulan_4+bulan_5+bulan_6+bulan_7+bulan_8+bulan_9+bulan_10+bulan_11+bulan_12) OVER (PARTITION BY k."kodeRekening" ORDER BY k."kodeRekening") as realisasi1_kel'), DB::raw('sum(coalesce(r."nilaiDetailTbp",0)) OVER (PARTITION BY k."kodeRekening" ORDER BY k."kodeRekening") realisasi2_kel'),
            DB::raw('sum(bulan_1+bulan_2+bulan_3+bulan_4+bulan_5+bulan_6+bulan_7+bulan_8+bulan_9+bulan_10+bulan_11+bulan_12) OVER (PARTITION BY j."kodeRekening" ORDER BY j."kodeRekening") as realisasi1_jenis'), DB::raw('sum(coalesce(r."nilaiDetailTbp",0)) OVER (PARTITION BY j."kodeRekening" ORDER BY j."kodeRekening") realisasi2_jenis'))
            ->whereIn('j.kodeRekening', ["4.1.01", "4.1.02"])
            ->groupBy('s.kodeRekening', 's.namaRekening', 'k.kodeRekening', 'k.namaRekening', 
            'j.kodeRekening', 'j.namaRekening', 
            'bulan_1', 'bulan_2', 'bulan_3', 'bulan_4', 'bulan_5', 'bulan_6', 'bulan_7', 'bulan_8', 'bulan_9', 'bulan_10', 'bulan_11', 'bulan_12', DB::raw('coalesce(r."nilaiDetailTbp",0)'))->distinct();

        $path = public_path() . "/json/beban.json";
        $json = json_decode(file_get_contents($path), true); 
        
        $bebanCollection = DB::table(DB::raw("(".$this->arrayToTable($json["beban"]).") as a"))
            ->select('kode_struk', 'nama_struk', 'kode_kel', 'nama_kel', 'kode_jenis', 'nama_jenis',  'realisasi1 as realisasi1_jenis', 'realisasi2 as realisasi2_jenis',
            DB::raw('sum(realisasi1) OVER (PARTITION BY kode_struk ORDER BY kode_struk) as realisasi1_struk'), DB::raw('sum(realisasi2) OVER (PARTITION BY kode_struk ORDER BY kode_struk) as realisasi2_struk'),
            DB::raw('sum(realisasi1) OVER (PARTITION BY kode_kel ORDER BY kode_kel) as realisasi1_kel'), DB::raw('sum(realisasi2) OVER (PARTITION BY kode_kel ORDER BY kode_kel) as realisasi2_kel'))->get();

            //dd($asetCollection);
            //return view('laporan.neraca', ["aset" => $asetCollection, "kewajiban" => $kewajibanCollection, "ekuitas" => $ekuitasCollection, "namaProvinsi" => $namaProvinsi, "periode" => $periode]);
        $pendapatanCollection = $pendapatanCollection->get();
        $namaFile = "operasional_".date("Y_m_d_H_i");
        if ($searchparams['formatFile']=="xls")
            return (new OperasionalExport(["pendapatan" => $pendapatanCollection, "beban" => $bebanCollection, "namaProvinsi" => $namaProvinsi, "periode" => $periode, "namaSkpd" => $namaSkpd]))->download($namaFile.'.xlsx');
        else if ($searchparams['formatFile']=="pdf")
            return PDF::loadView('laporan.operasional', ["pendapatan" => $pendapatanCollection, "beban" => $bebanCollection, "namaProvinsi" => $namaProvinsi, "periode" => $periode, "namaSkpd" => $namaSkpd])
                ->setOptions([
                    'dpi'              => 70,
                    'page-size'        => 'A4',
                    'margin-top'       => 15,
                    'margin-right'     => 15,
                    'margin-left'      => 15,
                    'margin-bottom'    => 15,
                    //'orientation'      => $orientation,
                    //'footer-right'     => ($enabledFooter) ? 'Halaman [page] dari [toPage]' : '',
                    'footer-font-size' => 6,
                    'footer-font-name' => 'Times New Roman',
                ])
                ->stream($namaFile.'.pdf');

    }

    public function cetakNeraca (Request $request) {
        $namaProvinsi = "PROVINSI JAWA BARAT";
        $periode = "TAHUN ANGGARAN 2021";
        $namaSkpd = "";
        $searchparams = json_decode($request->searchparams, true);
        $path = public_path() . "/json/neraca.json";
        $json = json_decode(file_get_contents($path), true); 
        
        $asetCollection = DB::table(DB::raw("(".$this->arrayToTable($json["aset"]).") as a"))
            ->select('kode_struk', 'nama_struk', 'kode_kel', 'nama_kel', 'kode_jenis', 'nama_jenis', 'kode_obyek', 'nama_obyek',  'realisasi1', 'realisasi2',
            DB::raw('sum(realisasi1) OVER (PARTITION BY kode_struk ORDER BY kode_struk) as realisasi1_struk'), DB::raw('sum(realisasi2) OVER (PARTITION BY kode_struk ORDER BY kode_struk) as realisasi2_struk'),
            DB::raw('sum(realisasi1) OVER (PARTITION BY kode_kel ORDER BY kode_kel) as realisasi1_kel'), DB::raw('sum(realisasi2) OVER (PARTITION BY kode_kel ORDER BY kode_kel) as realisasi2_kel'),
            DB::raw('sum(realisasi1) OVER (PARTITION BY kode_jenis ORDER BY kode_jenis) as realisasi1_jenis'), DB::raw('sum(realisasi2) OVER (PARTITION BY kode_jenis ORDER BY kode_jenis) as realisasi2_jenis'))->get();

        $kewajibanCollection = DB::table(DB::raw("(".$this->arrayToTable($json["kewajiban"]).") as a"))
            ->select('kode_struk', 'nama_struk', 'kode_kel', 'nama_kel', 'kode_jenis', 'nama_jenis', 'kode_obyek', 'nama_obyek',  'realisasi1', 'realisasi2',
            DB::raw('sum(realisasi1) OVER (PARTITION BY kode_struk ORDER BY kode_struk) as realisasi1_struk'), DB::raw('sum(realisasi2) OVER (PARTITION BY kode_struk ORDER BY kode_struk) as realisasi2_struk'),
            DB::raw('sum(realisasi1) OVER (PARTITION BY kode_kel ORDER BY kode_kel) as realisasi1_kel'), DB::raw('sum(realisasi2) OVER (PARTITION BY kode_kel ORDER BY kode_kel) as realisasi2_kel'),
            DB::raw('sum(realisasi1) OVER (PARTITION BY kode_jenis ORDER BY kode_jenis) as realisasi1_jenis'), DB::raw('sum(realisasi2) OVER (PARTITION BY kode_jenis ORDER BY kode_jenis) as realisasi2_jenis'))->get();

        $ekuitasCollection = DB::table(DB::raw("(".$this->arrayToTable($json["ekuitas"]).") as a"))
            ->select('kode_struk', 'nama_struk', 'kode_kel', 'nama_kel', 'kode_jenis', 'nama_jenis', 'kode_obyek', 'nama_obyek',  'realisasi1', 'realisasi2',
            DB::raw('sum(realisasi1) OVER (PARTITION BY kode_struk ORDER BY kode_struk) as realisasi1_struk'), DB::raw('sum(realisasi2) OVER (PARTITION BY kode_struk ORDER BY kode_struk) as realisasi2_struk'),
            DB::raw('sum(realisasi1) OVER (PARTITION BY kode_kel ORDER BY kode_kel) as realisasi1_kel'), DB::raw('sum(realisasi2) OVER (PARTITION BY kode_kel ORDER BY kode_kel) as realisasi2_kel'),
            DB::raw('sum(realisasi1) OVER (PARTITION BY kode_jenis ORDER BY kode_jenis) as realisasi1_jenis'), DB::raw('sum(realisasi2) OVER (PARTITION BY kode_jenis ORDER BY kode_jenis) as realisasi2_jenis'))->get();

            //dd($asetCollection);
            //return view('laporan.neraca', ["aset" => $asetCollection, "kewajiban" => $kewajibanCollection, "ekuitas" => $ekuitasCollection, "namaProvinsi" => $namaProvinsi, "periode" => $periode]);
        $namaFile = "neraca_".date("Y_m_d_H_i");
        if ($searchparams['formatFile']=="xls")
            return (new NeracaExport(["aset" => $asetCollection, "kewajiban" => $kewajibanCollection, "ekuitas" => $ekuitasCollection, "namaProvinsi" => $namaProvinsi, "periode" => $periode, "namaSkpd" => $namaSkpd]))->download($namaFile.'.xlsx');
        else if ($searchparams['formatFile']=="pdf")
            return PDF::loadView('laporan.neraca', ["aset" => $asetCollection, "kewajiban" => $kewajibanCollection, "ekuitas" => $ekuitasCollection, "namaProvinsi" => $namaProvinsi, "periode" => $periode, "namaSkpd" => $namaSkpd])
                ->setOptions([
                    'dpi'              => 70,
                    'page-size'        => 'A4',
                    'margin-top'       => 15,
                    'margin-right'     => 15,
                    'margin-left'      => 15,
                    'margin-bottom'    => 15,
                    //'orientation'      => $orientation,
                    //'footer-right'     => ($enabledFooter) ? 'Halaman [page] dari [toPage]' : '',
                    'footer-font-size' => 6,
                    'footer-font-name' => 'Times New Roman',
                ])
                ->stream($namaFile.'.pdf');

    }

    public function cetakJurnalumum (Request $request) {
        $namaProvinsi = "PROVINSI JAWA BARAT";
        $periode = "TAHUN ANGGARAN 2021";
        $namaSkpd = "1.01.01. - DINAS PENDIDIKAN";
        $searchparams = json_decode($request->searchparams, true);
        $path = public_path() . "/json/jurnalUmum.json";
        $json = json_decode(file_get_contents($path), true); 
        
        $jurnalumumCollection = DB::table(DB::raw("(".$this->arrayToTable($json["jurnalumum"]).") as a"))->get();

        $namaFile = "neraca_".date("Y_m_d_H_i");
        if ($searchparams['formatFile']=="xls")
            return (new JurnalumumExport(["jurnalumum" => $jurnalumumCollection, "namaProvinsi" => $namaProvinsi, "periode" => $periode, "namaSkpd" => $namaSkpd]))->download($namaFile.'.xlsx');
        else if ($searchparams['formatFile']=="pdf")
            return PDF::loadView('laporan.jurnalumum', ["jurnalumum" => $jurnalumumCollection, "namaProvinsi" => $namaProvinsi, "periode" => $periode, "namaSkpd" => $namaSkpd])
                ->setOptions([
                    'dpi'              => 70,
                    'page-size'        => 'A4',
                    'margin-top'       => 15,
                    'margin-right'     => 15,
                    'margin-left'      => 15,
                    'margin-bottom'    => 15,
                    //'orientation'      => $orientation,
                    //'footer-right'     => ($enabledFooter) ? 'Halaman [page] dari [toPage]' : '',
                    'footer-font-size' => 6,
                    'footer-font-name' => 'Times New Roman',
                ])
                ->stream($namaFile.'.pdf');

    }
    public function printLaporanToPDF($namaDokumen, $data, $orientation = null, $enabledFooter = true, $multiPageName = null)
    {
        if ($multiPageName != null) {
            $data = PDF::loadView('laporan.' . $namaDokumen, $data)
                ->setOptions([
                    'dpi'              => 80,
                    'page-size'        => 'A4',
                    'margin-top'       => 15,
                    'margin-right'     => 15,
                    'margin-left'      => 15,
                    'margin-bottom'    => 15,
                    'orientation'      => $orientation,
                    'footer-right'     => ($enabledFooter) ? 'Halaman [page] dari [toPage]' : '',
                    'footer-font-size' => 6,
                    'footer-font-name' => 'Times New Roman',
                ])
                ->stream($multiPageName . '.pdf');
        } else {
            $data = PDF::loadView('laporan.' . $namaDokumen, $data)
                ->setOptions([
                    'dpi'              => 80,
                    'page-size'        => 'A4',
                    'margin-top'       => 15,
                    'margin-right'     => 15,
                    'margin-left'      => 15,
                    'margin-bottom'    => 15,
                    'orientation'      => $orientation,
                    'footer-right'     => ($enabledFooter) ? 'Halaman [page] dari [toPage]' : '',
                    'footer-font-size' => 6,
                    'footer-font-name' => 'Times New Roman',
                ])
                ->stream($namaDokumen . '.pdf');
        }
        return $data;
    }

    public function test()
    {
        return $this->printLaporanToPDF('test', [
            'data' => 'Hello',
        ], 'portrait');
    }

    public function printPerubahanSaldoAnggaranLebih(Request $request)
    {
        /**
         * START
         * TODO: $tableRows seharusnya di-query dari DB
        */
        $tableRows = array();
        array_push($tableRows, (object) [
            'name' => 'Saldo Anggaran Lebih Awal',
            'before' => '',
            'after' => '',
        ]);
        array_push($tableRows, (object) [
            'name' => 'Penggunaan SAL sebagai Penerimaan Pembiayaan Tahun Berjalan',
            'before' => '',
            'after' => '',
            'children' => array(
                (object) [
                    'name' => 'Item 1',
                    'before' => '',
                    'after' => ''      
                ],
                (object) [
                    'name' => 'Item 2',
                    'before' => '',
                    'after' => ''      
                ],
            ),
            'subtotalBefore' => '',
            'subtotalAfter' => ''
        ]);
        array_push($tableRows, (object) [
            'name' => 'Lain-lain',
            'before' => '',
            'after' => '',
            'children' => array(
                (object) [
                    'name' => 'Saldo Anggaran Lebih Akhir',
                    'before' => '',
                    'after' => ''      
                ],
            ),
            'subtotalBefore' => '',
            'subtotalAfter' => ''
        ]);
        
        /**
         * END
        */
        $firstYear = $request->input('first_year');
        $secondYear = $request->input('second_year');

        $data = [
            'governmentUnitName' => 'Pemerintah Provinsi Jawa Barat',
            'firstYear' => $firstYear,
            'secondYear' => $secondYear,
            'tableRows' => $tableRows,
        ];
        return $this->printLaporanToPDF('perubahan-saldo-anggaran-lebih', $data, 'landscape');
    }
}
