<?php

namespace App\Http\Controllers\KebijakanAkuntansi;

use App\Http\Controllers\Controller;
use App\Models\JournalMode;
use App\Models\TransactionType;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KebijakanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search', false);
        $order  = $request->get('order', 'created_at');

        $kebijakanQuery = TransactionType::query()->whereHas('journalModes')
            ->select(['id', 'name'])
            ->with(['journalModes'=> function($mode){
                $mode->select(['id', 'code', 'name']);
            }]);
        
        if ($search) {
            $kebijakanQuery->where(function ($query) use ($search) {
                $query->where('name', 'ilike', '%' . $search . '%')
                    ->orWhere('code', 'ilike', '%' . $search . '%')
                    ->orWhere('description', 'ilike', '%' . $search . '%');
            });
        }

        if ($transactionTypeId = $request->get('transaction_type_id')) {
            $kebijakanQuery->where('id', $transactionTypeId);
        }

        if ($journalMode = $request->get('journal_mode_id')) {
            $kebijakanQuery->whereHas('journalModes', function($query) use ($journalMode){
                $query->wherePivot('journal_mode_id', $journalMode);
            });
        }

        $count = $kebijakanQuery->count();

        $page = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if ($order) {
            $orderDirection = $request->get('order_direction', 'asc');
            if (empty($orderDirection)) $orderDirection = 'asc';

            switch ($order) {
                default:
                    $kebijakanQuery->orderBy($order, $orderDirection);
                    break;
            }
        }

        $kebijakanCollection = $kebijakanQuery->skip(($page - 1) * $perpage)->take($perpage)->get();

        // format data
        foreach ($kebijakanCollection as &$model) {
            $mode = $model->journalModes->first();
            $user = User::query()->select('userName')->find($mode->pivot->created_by);

            $model->created_by =  $user ? $user->userName : null;
            $model->created_at = $mode->pivot->created_at;
        }

        $result = [
            'success'=> true,
            'message'=> 'Berhasil memuat daftar kebijakan akuntansi.',
            'data' => $kebijakanCollection,
            'count' => $count,
            'page'=> $page,
            'perpage'=> $perpage
        ];

        return response()->json($result);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'transaction_type_id'=> ['required', 'exists:transaction_types,id'],
            'journal_modes'=> ['required'],
            'journal_modes.*'=> ['exists:journal_modes,code'],
        ], $request->only(['transaction_type_id', 'journal_modes']));

        DB::beginTransaction();
        try {
            
            $settingKebijakan = TransactionType::find($request->transaction_type_id);

            foreach ($request->journal_modes as $mode) {
                $journalMode = JournalMode::whereCode($mode)->first();
                $createdById = auth()->user()->idUser;

                $settingKebijakan->journalModes()->attach($journalMode, ['created_by'=> $createdById]);
            }
            
            DB::commit();

            return response()->json([
                'success'=> true,
                'message'=> 'Berhasil membuat kebijakan.',
                'data'=> $settingKebijakan->load('journalModes')
            ]);

        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TransactionType  $settingKebijakan
     * @return \Illuminate\Http\Response
     */
    public function show(TransactionType $settingKebijakan)
    {
        $settingKebijakan = $settingKebijakan->load('journalModes');

        if (!$settingKebijakan->journalModes()->count()) {
            abort(422, 'Belum memiliki mode jurnal. Silahkan tambah data!');
        }

        return response()->json([
            'success'=> true,
            'message'=> 'Berhasil memuat kebijakan akuntansi.',
            'data'=> $settingKebijakan
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TransactionType  $settingKebijakan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TransactionType $settingKebijakan)
    {
        $request->validate([
            'journal_modes'=> ['required'],
            'journal_modes.*'=> ['exists:journal_modes,code'],
        ], $request->only(['journal_modes']));
        

        DB::beginTransaction();
        try {
            
            $userId = auth()->user()->idUser;
            $syncData = [];

            JournalMode::whereIn('code', $request->journal_modes)->pluck('id')
                ->map(function($modeId) use ($userId, &$syncData){
                    return $syncData[$modeId] = ['created_by'=> $userId];
                });

            $settingKebijakan->journalModes()->sync($syncData);
            
            DB::commit();

            return response()->json([
                'success'=> true,
                'message'=> 'Berhasil mengubah kebijakan.',
                'data'=> $settingKebijakan->load('journalModes')
            ]);


        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TransactionType  $settingKebijakan
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransactionType $settingKebijakan)
    {
        $settingKebijakan->journalModes()->detach();

        return response()->json([
            'success'=> true,
            'message'=> 'Berhasil menghapus kebijakan.',
            'data'=> null
        ]);
    }
}
