<?php

namespace App\Http\Controllers\KebijakanAkuntansi;

use App\Http\Controllers\Controller;
use App\Models\JenisBukti;
use App\Models\JournalMode;
use App\Models\Transaction;
use App\Models\TransactionType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TempJournalPenerimaanController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'transaction_type_id'=> ['nullable'],
            'journal_mode'=> ['required', 'exists:journal_modes,code'],
            'document_number'=> ['nullable'],
            'document_reference_id'=> ['required'],
            'jenis_bukti'=> ['required', 'in:SKP,SKR'],
            'amount'=> ['nullable'],
            'description'=> ['nullable'],
            'transaction_date'=> ['nullable'],
            'document_manifest'=> ['nullable'],
        ], $request->all());

        // get existing transaction
        if ($request->jenis_bukti === 'SKP') {
            $transaksiPenerimaan = DB::connection('pgsql_simda')->table('Penerimaan.Skp')
                ->select([
                    '*',
                    'nomorSkp as nomor',
                    'idSkp as document_id',
                    'nilaiSkp as jumlah',
                ])
                ->where('idSkp', $request->document_reference_id)
                ->first();
        }

        if ($request->jenis_bukti === 'SKR') {
            $transaksiPenerimaan = DB::connection('pgsql_simda')->table('Penerimaan.Skrd')
                ->select([
                    '*',
                    'nomorSkrd as nomor',
                    'idSkrd as document_id',
                    'nilaiSkrd as jumlah',
                    'tanggalSkrd as tanggal',
                ])
                ->where('idSkrd', $request->document_reference_id)
                ->first();
        }

        if (!$transaksiPenerimaan) {
            abort(422, 'Data transaksi penerimaan tidak ditemukan');
        }

        DB::beginTransaction();
        try {
            $journalMode = JournalMode::whereCode($request->journal_mode)->first();
            $jenisBukti = JenisBukti::whereCode(strtolower($request->jenis_bukti))->first();
            // $transactionType = TransactionType::find($request->transaction_type_id); SEMENTARA // TODO:

            $newTransaction = new Transaction([
                'document_number'=> $transaksiPenerimaan->nomor,
                'document_reference_id'=> $transaksiPenerimaan->document_id,
                'amount'=> $transaksiPenerimaan->jumlah,
                'description'=> null,
                'transaction_date'=> $transaksiPenerimaan->tanggal,
                'document_manifest'=> json_encode((array) $transaksiPenerimaan),
            ]);

            // $newTransaction->transactionType()->associate($transactionType);
            $newTransaction->jenisBukti()->associate($jenisBukti);
            $newTransaction->journalMode()->associate($journalMode)->save();
            
            DB::commit();

            return response()->json([
                'success'=> true,
                'message'=> 'Berhasil menambah kebijakan',
                'data'=> $newTransaction
            ]);

        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $settingKebijakan
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $settingKebijakan)
    {
        return response()->json($settingKebijakan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $settingKebijakan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $settingKebijakan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $settingKebijakan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $settingKebijakan)
    {
        //
    }
}
