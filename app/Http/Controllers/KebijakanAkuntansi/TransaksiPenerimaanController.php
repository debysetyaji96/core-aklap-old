<?php

namespace App\Http\Controllers\KebijakanAkuntansi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransaksiPenerimaanController extends Controller
{
    public function index(Request $request)
    {
        $params = $request->get('params', false) ? json_decode($request->params, true) : '';
        $search = $request->get('search', false);
        $order  = $request->get('order', 'tanggal_transaksi');

        $penerimaanSkpQuery = DB::connection('pgsql_simda')->table('Penerimaan.Skp')
            ->select([
                'idSkp as id',
                DB::raw("'SKP' as jenis_bukti"),
                DB::raw("case when 'isTbp' = '1' then 'TBP' else 'SKP' end as posisi"),
                'nomorSkp as nomor',
                'nomorSkp as nomor_bukti',
                'tanggal as tanggal_transaksi',
                'nilaiSkp as jumlah',
                'tahun',
                'idDaerah as id_daerah',
                'idSkpd as id_skpd',
            ])
            ->whereNotNull('idSkp')
            ->whereNull('idSkpTujuan');

        $penerimaanSkrdQuery = DB::connection('pgsql_simda')->table('Penerimaan.Skrd')
            ->select([
                'idSkrd as id',
                DB::raw("'SKR' as jenis_bukti"),
                DB::raw("case when 'isTbp' = '1' then 'TBP' else 'SKR' end as posisi"),
                'nomorSkrd as nomor',
                'nomorSkrd as nomor_bukti',
                'tanggalSkrd as tanggal_transaksi',
                'nilaiSkrd as jumlah',
                'tahun',
                'idDaerah as id_daerah',
                'idSkpd as id_skpd',
            ])
            ->whereNotNull('idSkrd');

        if ($search) {
            $penerimaanSkpQuery->where(function ($query) use ($search) {
                $query->where('nomorSkp', 'ilike', '%' . $search . '%');
            });

            $penerimaanSkrdQuery->where(function ($query) use ($search) {
                $query->where('nomorSkrd', 'ilike', '%' . $search . '%');
            });
        }

        if ($params) {
            foreach ($params as $key => $val) {
                if ($val !== false && ($val == '' || is_array($val) && count($val) == 0)) continue;
                switch ($key) {
                    case 'nomor':
                        $penerimaanSkpQuery->where('nomorSkp', $val);
                        $penerimaanSkrdQuery->where('nomorSkrd', $val);
                        break;
                    case 'id_skpd':
                        $penerimaanSkpQuery->where('idSkpd', $val);
                        $penerimaanSkrdQuery->where('idSkpd', $val);
                        break;
                    case 'id_daerah':
                        $penerimaanSkpQuery->where('idDaerah', $val);
                        $penerimaanSkrdQuery->where('idDaerah', $val);
                        break;
                    case 'tahun':
                        $penerimaanSkpQuery->where('tahun', $val);
                        $penerimaanSkrdQuery->where('tahun', $val);
                        break;
                    default:
                        break;
                }
            }
        }

        // UNION
        $penerimaanSkrdQuery->union($penerimaanSkpQuery);

        $count = $penerimaanSkrdQuery->count();

        $page = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if ($order) {
            $orderDirection = $request->get('order_direction', 'asc');
            if (empty($orderDirection)) $orderDirection = 'asc';

            switch ($order) {
                case 'nomor':
                    $penerimaanSkrdQuery->orderBy('nomor', $orderDirection);
                    break;
                case 'jumlah':
                    $penerimaanSkrdQuery->orderBy('jumlah', $orderDirection);
                    break;
                default:
                    $penerimaanSkrdQuery->orderBy($order, $orderDirection);
                    break;
            }
        }

        $penerimaanSkrdQuery = $penerimaanSkrdQuery->skip(($page - 1) * $perpage)->take($perpage)->get();

        // format data
        foreach ($penerimaanSkrdQuery as &$model) {
            #
        }

        $result = [
            'success'=> true,
            'message'=> 'Berhasil memuat daftar transaksi penerimaan',
            'data' => $penerimaanSkrdQuery,
            'count' => $count,
            'page'=> $page,
            'perpage'=> $perpage
        ];

        return response()->json($result);


    }
}
