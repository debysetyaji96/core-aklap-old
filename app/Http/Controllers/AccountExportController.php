<?php

namespace App\Http\Controllers;

use App\Exports\AccountsExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;

class AccountExportController extends Controller
{
    public function export(Excel $excel)
    {
        return $excel->download(new AccountsExport, 'chart-of-accounts.xlsx');
    }
}
