<?php

namespace App\Http\Controllers\JurnalPendapatan;

use App\Http\Controllers\Controller;
use App\Models\Journal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuJurnalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search', false);
        $order  = $request->get('order', 'posted_at');

        $journalsQuery = Journal::query()->whereHas('journalDetails')
            ->select([
                'id',
                DB::raw("case when length(description) > 70 then left(description, 30) || '...' else description end as description"),
                'transaction_id'
            ])
            ->where('status', 'posted')
            ->with([
                'journalDetails'=> function($journalDetail){
                    $journalDetail->with('account');
                        
                },
                'transaction'=> function($transaction){
                    $transaction->with([
                        'jenisBukti'=> function ($jenisBukti){
                            $jenisBukti->select(['id', 'code', 'name']);
                        },
                        'journalMode'=> function ($journalMode){
                            $journalMode->select(['id', 'code', 'name']);
                        }, 
                        'transactionType'=> function ($transactionType){
                            $transactionType->select(['id', 'name', 'journal_type_id'])->with(['journalType']);
                        },
                    ])
                    ->select(['id', 'document_number', 'jenis_bukti_id', 'journal_mode_id', 'transaction_type_id']);
                }
            ]);

        // Search by keyword
        if ($search) {
            $journalsQuery->where(function ($query) use ($search) {
                $query->whereHas('transaction', function ($transaction) use ($search){
                    $transaction->where('description', 'ilike', '%' . $search . '%')
                        ->orWhere('document_number', 'ilike', '%' . $search . '%');
                })
                ->orWhereHas('journalDetails', function ($journalDetails) use ($search){
                    $journalDetails->where('description', 'ilike', '%' . $search . '%');
                });
            });
        }

        // Filter
        if ($startDate = $request->get('start_date')) {
            $journalsQuery->where('posted_at', '>=', $startDate);
        }
        if ($endDate = $request->get('end_date')) {
            $journalsQuery->where('posted_at', '<=', $endDate);
        }
        if ($type = $request->get('journal_type_id')) {
            $journalsQuery->whereHas('transaction', function($transaction) use ($type){
                $transaction->whereHas('transactionType', function ($transactionType) use ($type){
                    $transactionType->whereHas('journalType', function ($journalType) use ($type){
                        $journalType->where('id', $type);
                    });
                });
            });
        }

        $count = $journalsQuery->count();

        $page = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        $page = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        // Order
        if ($order) {
            $orderDirection = $request->get('order_direction', 'desc');
            switch ($order) {
                default:
                    $journalsQuery->orderBy($order, $orderDirection);
                    break;
            }
        }

        $resultCollection = $journalsQuery->skip(($page - 1) * $perpage)->take($perpage)->get();

        // format data
        foreach ($resultCollection as &$model) {
            $model->document_number = $model->transaction->document_number;
            $model->journal_type_code = strtoupper($model->transaction->transactionType->journalType->code);
            $model->journal_mode = $model->transaction->journalMode;
        }

        $response = [
            'success'=> true,
            'message'=> 'Berhasil memuat daftar journal.',
            'data' => $resultCollection,
            'count' => $count,
            'page'=> $page,
            'perpage'=> $perpage
        ];

        return response()->json($response);
    }
}
