<?php

namespace App\Http\Controllers\JurnalPendapatan;

use App\Exports\TransaksiPendapatanExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;

class ExportController extends Controller
{
    public function exportTransaksi(Request $request, Excel $excel)
    {
        $timestamp = now()->timestamp;
        $fileName = "trx-pendapatan-$timestamp.xlsx";

        $params = $request->only(['start_date', 'end_date', 'order', 'order_direction', 'search']);

        return $excel->download(new TransaksiPendapatanExport($params), $fileName);
    }
}
