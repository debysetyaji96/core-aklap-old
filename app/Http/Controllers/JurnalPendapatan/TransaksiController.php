<?php

namespace App\Http\Controllers\JurnalPendapatan;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search', false);
        $order  = $request->get('order', 'transaction_date');

        $transactionQuery = Transaction::query()->whereHas('transactionType')
            ->whereHas('journalMode')
            ->whereHas('jenisBukti', function($jenisBukti){
                $jenisBukti->whereIn('code', ['tbp', 'skp', 'skr']);
            })
            // ->whereHas('journals') // TODO: Must be specified
            ->select(['id', 'document_number', 
                DB::raw("round(amount::double precision) as amount"), 
                'transaction_date', 'journal_mode_id', 'jenis_bukti_id', 
                DB::raw("case when length(description) > 0 then left(description, 20) || '...' else '' end as description"),
            ])
            ->with([
                'journalMode'=> function ($mode){
                    $mode->select(['id', 'code', 'name']);
                },
                'jenisBukti'=> function ($jenisBukti) {
                    $jenisBukti->select(['id', 'code', 'name']);
                },
            ]);

        // Search by keyword
        if ($search) {
            $transactionQuery->where(function ($query) use ($search) {
                $query->where('document_number', 'ilike', '%' . $search . '%')
                    ->orWhere('description', 'ilike', '%' . $search . '%');
            });
        }

        // Filter by params
        if ($startDate = $request->get('start_date')) {
            $transactionQuery->where('transaction_date', '>=', $startDate);
        }

        if ($endDate = $request->get('end_date')) {
            $transactionQuery->where('transaction_date', '<=', $endDate);
        }

        $count = $transactionQuery->count();

        $page = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        $page = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        // Order
        if ($order) {
            $orderDirection = $request->get('order_direction', 'desc');
            switch ($order) {
                case 'amount':
                    $transactionQuery->orderBy('amount', $orderDirection);
                    break;
                case 'document_number':
                    $transactionQuery->orderBy('document_number', $orderDirection);
                    break;
                default:
                    $transactionQuery->orderBy($order, $orderDirection);
                    break;
            }
        }

        $resultCollection = $transactionQuery->skip(($page - 1) * $perpage)->take($perpage)->get();

        // format data
        foreach ($resultCollection as &$model) {
            #
        }

        $response = [
            'success'=> true,
            'message'=> 'Berhasil memuat daftar transaksi.',
            'data' => $resultCollection,
            'count' => $count,
            'page'=> $page,
            'perpage'=> $perpage
        ];

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        $response = $transaction->load(['jenisBukti', 'transactionType', 'journalMode']);

        return response()->json([
            'success'=> true,
            'message'=> 'Berhasil memuat data transaksi.',
            'data'=> $response
        ]);
    }
}
