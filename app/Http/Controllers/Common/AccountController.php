<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Models\Account;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $models = Account::query();
        $searchparams = json_decode($request->searchparams, true);

        /*if ($user->daerah && !$user->daerah->is_pusat) {
            if ($user->daerah->is_prop) {
                $models->where('is_provinsi',true);
            } else {
                $models->where('is_kabkota',true);
            }
        }*/
        if ($searchparams && is_array($searchparams)) {
            foreach ($searchparams as $key => $value) {
                switch ($key) {
                    case 'kodeRekeningBeginsWith':
                        $models->where('code','ilike',$value.'%');
                        break;
                    case 'level_rekening':
                        $models->where('level_rekening',$value);
                        break;
                    case 'is_lo':
                        $models->where(function ($query) {
                            $query->where('code', 'ilike', '1.%')
                                  ->orWhere('code', 'ilike', '2.%')
                                  ->orWhere('code', 'ilike', '3.%')
                                  ->orWhere('code', 'ilike', '7.%')
                                  ->orWhere('code', 'ilike', '8.%');
                        });
                        break;
                }
            }
        }
        $models = $models->get();
        return response()->json([
            'success' => true,
            'data' => $models
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        return response()->json([
            'success' => true,
            'model' => $account
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        //
    }
}
