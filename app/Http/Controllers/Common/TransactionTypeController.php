<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Models\TransactionType;
use Illuminate\Http\Request;

class TransactionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search', false);
        $order  = $request->get('order', 'created_at');

        $typeCollection = TransactionType::query()->select('id', 'code', 'name');

        if ($search) {
            $typeCollection->where(function ($query) use ($search) {
                $query->where('code', 'ilike', '%' . $search . '%')
                    ->orWhere('name', 'ilike', '%' . $search . '%');
            });
        }

        if ($request->get('kebijakan')) {
            $typeCollection->whereDoesntHave('journalModes');
        }

        $count = $typeCollection->count();

        $page = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if ($order) {
            $orderDirection = $request->get('order_direction', 'asc');
            if (empty($orderDirection)) $orderDirection = 'asc';

            switch ($order) {
                case 'code':
                    $typeCollection->orderBy('code', $orderDirection);
                    break;
                case 'name':
                    $typeCollection->orderBy('name', $orderDirection);
                    break;
                default:
                    $typeCollection->orderBy($order, $orderDirection);
                    break;
            }
        }

        $typeCollection = $typeCollection->skip(($page - 1) * $perpage)->take($perpage)->get();

        $result = [
            'success'=> true,
            'message'=> 'Berhasil memuat daftar Tipe Transaksi',
            'data' => $typeCollection,
            'count' => $count,
            'page'=> $page,
            'perpage'=> $perpage
        ];

        return response()->json($result);
    }
}
