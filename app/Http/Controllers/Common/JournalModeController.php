<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Models\JournalMode;
use Illuminate\Http\Request;

class JournalModeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search', false);
        $order  = $request->get('order', 'created_at');

        $modeCollection = JournalMode::query()->select('id', 'code', 'name');

        if ($search) {
            $modeCollection->where(function ($query) use ($search) {
                $query->where('code', 'ilike', '%' . $search . '%')
                    ->orWhere('name', 'ilike', '%' . $search . '%');
            });
        }

        $count = $modeCollection->count();

        $page = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if ($order) {
            $orderDirection = $request->get('order_direction', 'asc');
            if (empty($orderDirection)) $orderDirection = 'asc';

            switch ($order) {
                default:
                    $modeCollection->orderBy($order, $orderDirection);
                    break;
            }
        }

        $modeCollection = $modeCollection->skip(($page - 1) * $perpage)->take($perpage)->get();

        $result = [
            'success'=> true,
            'message'=> 'Berhasil memuat daftar Mode Jurnal',
            'data' => $modeCollection,
            'count' => $count,
            'page'=> $page,
            'perpage'=> $perpage
        ];

        return response()->json($result);
    }
}
