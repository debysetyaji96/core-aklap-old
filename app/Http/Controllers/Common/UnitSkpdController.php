<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Skpd;
use Illuminate\Support\Facades\DB;

class UnitSkpdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $params = $request->get('params', false) ? json_decode($request->params, true) : '';
        $search = $request->get('search', false);
        $order  = $request->get('order');

        $models = Skpd::query()->select('id_skpd', 'kode_skpd', 'nama_skpd')
            ->where('is_skpd', 0)
            ->where('is_locked', 0);
        if ($user->daerah && !$user->daerah->is_pusat) {
            $models->where('id_daerah',$user->idDaerah);
        }

        if ($params) {
            foreach ($params as $key => $val) {
                if ($val !== false && ($val == '' || is_array($val) && count($val) == 0)) continue;
                switch ($key) {
                    case 'is_pendapatan':
                        $models->where('is_pendapatan', $val);
                        break;
                    case 'id_unit':
                        $models->where('id_unit', $val);
                        break;
                    default:
                        break;
                }
            }
        }

        $count = $models->count();

        if ($order) {
            $orderDirection = $request->get('order_direction', 'asc');
            if (empty($orderDirection)) $orderDirection = 'asc';

            switch ($order) {
                default:
                    $models->orderBy($order, $orderDirection);
                    break;
            }
        } else {
            $models->orderBy('nama_skpd', 'asc');
        }

        $models = $models->get();

        // format data
        foreach ($models as &$model) {
            #
        }

        $result = [
            'success'=> true,
            'message'=> 'Berhasil memuat daftar Unit SKPD',
            'data' => $models,
            'count' => $count,
        ];

        return response()->json($result);
    }
}
