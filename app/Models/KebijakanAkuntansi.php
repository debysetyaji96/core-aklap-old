<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class KebijakanAkuntansi extends Pivot
{
    protected $table = 'kebijakan_akuntansi';
}
