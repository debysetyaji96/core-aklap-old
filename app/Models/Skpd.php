<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Skpd extends Model
{
    protected $connection= 'pgsql_simda';
    protected $table = 'Referensi.r_skpd';

    public $incrementing = false;
    public $timestamps = false;

    protected $casts = [
        'id_skpd' => 'string',
        'is_skpd' => 'boolean',
    ];

    public function getParentAttribute()
    {
        if ($this->id_skpd == $this->id_unit) return null;
        return static::where('id_daerah', $this->id_daerah)->where('id_skpd', $this->id_unit)->first();
    }
}
