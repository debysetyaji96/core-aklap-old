<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JurnalPenyesuaian extends Model
{
	protected $table = "jurnal_penyesuaian";
	protected $primaryKey = "id";
	protected $fillable = [
		'nomor_bukti',
        'id_skpd',
        'id_daerah',
		'tanggal',
		'nama_kegiatan',
		'nama_subkegiatan',
		'keterangan',
	];
	
	public function skpd()
	{
		return $this->belongsTo('App\Models\Skpd', 'id_skpd', 'id_skpd')->where('id_daerah',$this->id_daerah);
	}

	public function detailJurnal()
	{
		return $this->hasMany('App\Models\JurnalPenyesuaianDetail', 'jurnal_penyesuaian_id', 'id');
	}
}
