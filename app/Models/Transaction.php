<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'document_number',
        'document_reference_id',
        'amount',
        'description',
        'transaction_date',
        'document_manifest',
    ];

    protected $dates = [
        'transaction_date'
    ];

    protected $casts = [
        'document_manifest'=> 'json'
    ];

    public function journalMode()
    {
        return $this->belongsTo(JournalMode::class, 'journal_mode_id');
    }

    public function transactionType()
    {
        return $this->belongsTo(TransactionType::class, 'transaction_type_id');
    }

    public function jenisBukti()
    {
        return $this->belongsTo(JenisBukti::class, 'jenis_bukti_id');
    }

    public function journals()
    {
        return $this->hasMany(Journal::class, 'transaction_id');
    }
}
