<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Daerah extends Model
{
    protected $connection= 'pgsql_simda';
    protected $table = "Referensi.r_daerah";
    protected $primaryKey = "id_daerah";

    // daftar provinsi yang digunakan untuk pemilihan BankServiceProvider
    // tergabung ke dalam vendor OY
    public const PROV_ACEH = 116;
    public const PROV_SUMBAR = 180;
    public const PROV_RIAU = 202;
    public const PROV_KEPRI = 519;
    public const PROV_SUMSEL = 229;
    public const PROV_BABEL = 501;
    public const PROV_BANTEN = 492;
    public const PROV_JABAR = 8;
    public const PROV_JATENG = 35;
    public const PROV_DIY = 71;
    public const PROV_JATIM = 77;
    public const PROV_KALSEL = 291;
    public const PROV_SULSEL = 346;
    public const PROV_SULBAR = 540;
    public const PROV_SULTENG = 334;
    public const PROV_SULUT = 318;
    public const PROV_GORONTALO = 510;
    public const PROV_NTT = 418;
    public const PROV_PAPUA_BARAT = 528;
    public const PROV_PAPUA = 440;

    // provinsi lainnya
    public const PROV_SUMUT = 141;
    public const PROV_JAMBI = 216;
    public const PROV_BENGKULU = 471;
    public const PROV_LAMPUNG = 246;
    public const PROV_DKI_JAKARTA = 1;
    public const PROV_BALI = 396;
    public const PROV_NTB = 407;
    public const PROV_KALBAR = 261;
    public const PROV_KALTENG = 276;
    public const PROV_KALTIM = 307;
    public const PROV_KALTARA = 546;
    public const PROV_SULTRA = 371;
    public const PROV_MALUKU = 384;
    public const PROV_MALUT = 482;

    protected $casts = [
        'is_pusat' => 'boolean',
        'is_prop' => 'boolean',
    ];
    public function getLogoUrlAttribute()
    {
        return asset('img/logo_daerah/'.$this->logo);
    }
}
