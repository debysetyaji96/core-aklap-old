<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = "accounts";
    protected $primaryKey = "id";

    public function accountType()
    {
        return $this->belongsTo(AccountType::class);
    }
}
