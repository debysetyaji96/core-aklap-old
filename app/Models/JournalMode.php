<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JournalMode extends Model
{
    protected $hidden = ['pivot'];

    public function transactionTypes()
    {
        return $this->belongsToMany(TransactionType::class, 'kebijakan_akuntansi', 'journal_mode_id', 'transaction_type_id')
            ->using(KebijakanAkuntansi::class)
            ->withTimestamps()
            ->withPivot([
                'created_by'
            ]);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'journal_mode_id');
    }
}
