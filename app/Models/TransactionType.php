<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
    
    public function journalModes()
    {
        return $this->belongsToMany(JournalMode::class, 'kebijakan_akuntansi', 'transaction_type_id', 'journal_mode_id')
            ->using(KebijakanAkuntansi::class)
            ->withTimestamps()
            ->withPivot([
                'created_by'
            ]);
    }

    public function journalType()
    {
        return $this->belongsTo(JournalType::class, 'journal_type_id');
    }
}
