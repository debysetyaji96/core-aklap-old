<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $connection = 'pgsql_simda';
    protected $table = "Referensi.Pegawai";
    protected $primaryKey = "idPegawai";
    public $timestamps = true;
    protected $fillable = [
        'namaPegawai',
        'tahunPegawai',
        'idSubUnit',
        'idJabatan',
        'idSkpd',
        'nipPegawai',
        'idSkpd',
        'noHP',
        'npwp',
        'pangkat',
        'golongan',
        'bankPembayar',
        'nomorRekening',
        'is_from_generate',
        'kodeBank',
        'alamat',
    ];

	public function jabatan()
	{
		return $this->belongsTo(Jabatan::class, 'idJabatan');
	}

}
