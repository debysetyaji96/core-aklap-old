<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SimdaSkpd extends Model
{
    protected $connection = 'pgsql_simda';

    protected $table = 'Referensi.v_skpd_rekap_indo';
    // protected $table = 'Referensi.r_skpd';

    protected $primaryKey = 'id_skpd';
}
