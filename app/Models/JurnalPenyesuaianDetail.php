<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JurnalPenyesuaianDetail extends Model
{
	protected $table = "jurnal_penyesuaian_detail";
	protected $primaryKey = "id";
	protected $fillable = [
        'jurnal_penyesuaian_id',
		'account_id',
		'debet',
		'kredit'
	];
	
	public function account()
	{
		return $this->belongsTo('App\Models\Account', 'accountid', 'id');
	}
	public function jurnal()
	{
		return $this->belongsTo('App\Models\JurnalPenyesuaian', 'jurnal_penyesuaian_id', 'id');
	}
}
