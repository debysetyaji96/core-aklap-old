<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class uniqueCode implements Rule
{
    protected $value;    
    protected $filter;
    protected $modelTable;
    protected $msg;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($model, $filter=[], $msg=null)
    {
        $this->modelTable = $model->getTable();
        if (!is_null($model->getKey())){ //Edit Data
            array_push($filter, [$model->getKeyName(),"!=", $model->getKey()]);   
        }
        $this->filter = $filter;
        $this->msg = $msg;
        
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->value = $value;
        $arratt = explode(".",$attribute);
        if (count($arratt)>1) $attribute = $arratt[count($arratt)-1];
        $filter = [[$attribute,"=",$value]];
        if (count($this->filter)>0){
            foreach ($this->filter as $arr) {
                array_push($filter, $arr);
            }
        }
        return !DB::table($this->modelTable)->where($filter)->first();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if (is_null($this->msg))
            return ":attribute ini sudah ada sebelumnya";
        else
            return $this->msg;
    }
}
