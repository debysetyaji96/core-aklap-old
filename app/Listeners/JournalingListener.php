<?php

namespace App\Listeners;

use App\Events\AutomaticJournalEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class JournalingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AutomaticJournalEvent  $event
     * @return void
     */
    public function handle(AutomaticJournalEvent $event)
    {
        $transaction = $event->transaction;
    }
}
