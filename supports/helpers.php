<?php
use Illuminate\Support\Str;


function read_coa_csv(): \Illuminate\Support\Collection {
    $filePath = database_path('seeds/csv') . "/accounts.csv";

    if (!file_exists($filePath)) {
        throw new \Exception("File doesn't exist");
    }

    $collection = collect();

    $handler = fopen($filePath, "r");
    $index = 0;

    while($line = fgetcsv($handler, 0, ';')) {
        list($digit1, $digit2, $digit3, $digit4, $digit5, $digit6, $name, $position) = $line;
        
        if ($index > 0) {
            $collection->push([
                'code'=> "{$digit1}.{$digit2}.{$digit3}.{$digit4}.{$digit5}.{$digit6}",
                'name'=> $name,
                // 'position'=> $position,
                // 'code' => Str::limit(preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u','',trim($code)), 25, null),
                // 'name' => rtrim(trim($name)),
                // 'type' => Str::limit(trim($type), 12, null),
                // 'parent' => Str::limit(trim($parent), 12, null),
                // 'level' => intval($level),
                // 'position' => rtrim(trim($position)) == "K" ? "credit" : "debit",
                // 'account_category' => rtrim(trim($tipeArusKas)),
                // 'cash_account' => rtrim(trim($cashAccount))
            ]);
        }

        $index += 1;
    }

    fclose($handler);

    return $collection;
}

function read_transaction_types_csv(): \Illuminate\Support\Collection {
    $filePath = database_path('seeds/csv') . "/transaction_types.csv";

    if (!file_exists($filePath)) {
        throw new \Exception("File doesn't exist");
    }

    $collection = collect();

    $handler = fopen($filePath, "r");
    $index = 0;

    while($line = fgetcsv($handler, 0, ';')) {
        list($code, $name, $description, $journalType, $sourcePpkdSpkd) = $line;
        
        if ($index > 0) {
            $collection->push([
                'code'=> "{$code}",
                'name'=> $name,
                'description'=> $description,
                'journal_type'=> strtolower(rtrim(trim($journalType))),
                'sumber'=> strtolower(rtrim(trim($sourcePpkdSpkd))),
            ]);
        }

        $index += 1;
    }

    fclose($handler);

    return $collection;
}

function read_transaction_mapping_csv($sheetCode = 'skpd'): \Illuminate\Support\Collection {
    $filePath = database_path('seeds/csv') . "/transaction_mapping_{$sheetCode}.csv";

    if (!file_exists($filePath)) {
        throw new \Exception("File doesn't exist");
    }

    $collection = collect();

    $handler = fopen($filePath, "r");
    $index = 0;

    while($line = fgetcsv($handler, 0, ';')) {
        list($tipeJurnal, $kodeTransaksi, $namaTransaksi, $deskripsiTransaksi, $jenisAkunDebit, $debitDigit1, $debitDigit2, $debitDigit3, $debitDigit4, $debitDigit5, $debitDigit6, $debitNamaAkun, $jenisAkunCredit, $creditDigit1, $creditDigit2, $creditDigit3, $creditDigit4, $creditDigit5, $creditDigit6, $creditNamaAkun) = $line;
        
        if ($index > 0) {
            $collection->push([
                'tipe_jurnal'=> strtolower(rtrim(trim($tipeJurnal))),
                'kode_transaksi'=> rtrim(trim($kodeTransaksi)),
                'jenis_akun_debit'=> strtolower(rtrim(trim($jenisAkunDebit))),
                'debit_kode_rekening'=> "{$debitDigit1}.{$debitDigit2}.{$debitDigit3}.{$debitDigit4}.{$debitDigit5}.{$debitDigit6}",
                'jenis_akun_credit'=> strtolower(rtrim(trim($jenisAkunCredit))),
                'credit_kode_rekening'=> "{$creditDigit1}.{$creditDigit2}.{$creditDigit3}.{$creditDigit4}.{$creditDigit5}.{$creditDigit6}",
            ]);
        }

        $index += 1;
    }

    fclose($handler);

    return $collection;
}