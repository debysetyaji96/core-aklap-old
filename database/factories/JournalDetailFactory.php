<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Account;
use App\Models\Journal;
use App\Models\JournalDetail;
use Faker\Generator as Faker;

$factory->define(JournalDetail::class, function (Faker $faker) {

    $account = Account::query()->inRandomOrder()->select('id')->first();
    // $journal = Journal::query()->whereDoesntHave('journalDetails')
    //     ->inRandomOrder()
    //     ->select('id')
    //     ->first();
        
    return [
        // 'journal_id'=> $journal->getAttribute('id'),
        'account_id'=> $account->getAttribute('id'),
        'position'=> 'debit',
        'amount'=> 0,
        'description'=> $faker->realText(100),
    ];
});
