<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Account;
use App\Models\Journal;
use App\Models\JournalDetail;
use App\Models\Transaction;
use Faker\Generator as Faker;

$factory->define(Journal::class, function (Faker $faker) {

    $transaction = Transaction::query()->whereDoesntHave('journals')
        ->select(['id', 'transaction_date', 'amount'])
        ->inRandomOrder()
        ->first();

    return [
        'transaction_id'=> $transaction->getAttribute('id'),
        'code'=> $faker->uuid,
        'status'=> 'posted',
        'description'=> $faker->realText(100),
        'posted_at'=> $faker->dateTimeBetween($transaction->getAttribute('transaction_date'), 'now'),
    ];
});

$factory->afterCreating(Journal::class, function(Journal $journal, $faker) {

    $accounts = Account::query()->select('id')->inRandomOrder()->limit(2)->get();
    $transactionAmount = $journal->transaction->amount;

    $journal->journalDetails()->save(factory(JournalDetail::class)->make([
        'journal_id'=> $journal->getAttribute('id'),
        'amount'=> $transactionAmount,
        'position'=> 'debit',
        'account_id'=> $accounts[0]->getAttribute('id')
    ]));

    $journal->journalDetails()->save(factory(JournalDetail::class)->make([
        'journal_id'=> $journal->getAttribute('id'),
        'amount'=> $transactionAmount,
        'position'=> 'credit',
        'account_id'=> $accounts[1]->getAttribute('id')
    ]));
});
