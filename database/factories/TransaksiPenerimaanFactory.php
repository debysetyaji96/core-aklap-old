<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\JenisBukti;
use App\Models\JournalMode;
use App\Models\Transaction;
use App\Models\TransactionType;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {

    $tipeTransaksi = TransactionType::inRandomOrder()->first();
    $jenisBukti = JenisBukti::inRandomOrder()->first();
    $journalMode = JournalMode::inRandomOrder()->first();

    return [
        'document_number'=> $faker->isbn13,
        'document_reference_id'=> null,
        'amount'=> $faker->numberBetween(10000000, 5000000000),
        'description'=> $faker->realText(300),
        'transaction_date'=> $faker->dateTimeBetween('-1 years', 'now', 'Asia/Jakarta'),
        'document_manifest'=> json_encode($faker->creditCardDetails),

        'transaction_type_id'=> $tipeTransaksi->id,
        'journal_mode_id'=> $journalMode->id,
        'jenis_bukti_id'=> $jenisBukti->id,
    ];
});
