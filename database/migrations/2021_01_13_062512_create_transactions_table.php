<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('transaction_type_id')->nullable();
            $table->unsignedInteger('journal_mode_id');
            $table->string('document_number', 50)->index();
            $table->string('document_reference_id')->index()->nullable();
            $table->unsignedInteger('jenis_bukti_id');
            $table->double('amount');
            $table->text('description')->nullable();
            $table->dateTime('transaction_date');
            $table->json('document_manifest')->nullable();
            $table->timestamps();

            $table->foreign('transaction_type_id')->references('id')->on('transaction_types'); // anggaran-1, pendapatan-3, ..,
            $table->foreign('journal_mode_id')->references('id')->on('journal_modes'); // A, M, R, -
            $table->foreign('jenis_bukti_id')->references('id')->on('jenis_bukti'); // TBP, SKP, SKR
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
