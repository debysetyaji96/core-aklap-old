<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKebijakanAkuntansiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kebijakan_akuntansi', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_type_id');
            $table->unsignedBigInteger('journal_mode_id');
            $table->unsignedInteger('created_by')->index();
            $table->timestamps();

            $table->foreign('transaction_type_id')->references('id')->on('transaction_types');
            $table->foreign('journal_mode_id')->references('id')->on('journal_modes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kebijakan_akuntansi');
    }
}
