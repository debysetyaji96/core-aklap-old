<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkpdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skpd', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->index()->unique();
            $table->unsignedInteger('id_skpd_simda');
            $table->string('kode_skpd');
            $table->string('nama_skpd');
            $table->unsignedInteger('id_daerah')->nullable();
            $table->unsignedInteger('tahun')->nullable();
            // $table->unsignedInteger('id_unit')->nullable();
            // $table->integer('is_locked')->nullable();
            // $table->integer('is_pendapatan')->nullable();
            // $table->integer('is_ppkd')->nullable();
            // $table->integer('is_skpd')->nullable();
            // $table->integer('id_daerah')->nullable();
            // $table->string('kode_skpd')->nullable();
            // $table->string('kode_skpd_lama')->nullable();
            // $table->string('kode_unit')->nullable();
            // $table->string('komisi')->nullable();
            // $table->string('nama_bendahara')->nullable();
            // $table->string('nama_kepala')->nullable();
            // $table->string('nama_skpd')->nullable();
            // $table->string('nip_bendahara')->nullable();
            // $table->string('nip_kepala')->nullable();
            // $table->string('pangkat_kepala')->nullable();
            // $table->integer('set_input')->nullable();
            // $table->string('status_kepala')->nullable();
            // $table->unsignedInteger('tahun')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skpd');
    }
}
