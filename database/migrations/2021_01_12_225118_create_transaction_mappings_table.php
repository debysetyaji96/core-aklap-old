<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_mappings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('journal_type_id')->index()->nullable();
            $table->unsignedInteger('transaction_type_id');
            $table->string('debit_category');
            $table->unsignedBigInteger('debit_account_id')->index()->nullable();
            $table->string('credit_category');
            $table->unsignedBigInteger('credit_account_id')->index()->nullable();
            $table->string('mapping_type', 20)->default('skpd');
            $table->timestamps();

            $table->foreign('journal_type_id')->references('id')->on('journal_types');
            $table->foreign('transaction_type_id')->references('id')->on('transaction_types');
            $table->foreign('debit_account_id')->references('id')->on('accounts');
            $table->foreign('credit_account_id')->references('id')->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_mappings');
    }
}
