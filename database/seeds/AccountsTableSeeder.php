<?php

use App\Models\Account;
use App\Models\AccountType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        Account::truncate();

        try {

            foreach (read_coa_csv() as $item) {
                $code = explode('.', $item['code']);

                $accountType = AccountType::where('code', 'like', $code[0] . '.%')->first();

                $existingAccount = Account::whereCode($item['code'])->first();

                if ($existingAccount) {
                    continue;
                }

                $newAccount = new Account(\Illuminate\Support\Arr::only($item, ['code', 'name'])); // 'position', 'level'

                $newAccount->accountType()->associate($accountType)->save();
            }


            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
