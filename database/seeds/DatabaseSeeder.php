<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(JournalTypesTableSeeder::class);
        $this->call(AccountTypesTableSeeder::class);
        // $this->call(AccountsTableSeeder::class);
        $this->call(TransactionTypesTableSeeder::class);
        $this->call(TransactionMappingPpkdTableSeeder::class);
        $this->call(TransactionMappingSkpdTableSeeder::class);
        $this->call(JenisBuktiTableSeeder::class);
        $this->call(JournalModesTableSeeder::class);
        
        // $this->call(UsersTableSeeder::class);
    }
}
