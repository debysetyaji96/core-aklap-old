<?php

use App\Models\JournalType;
use App\Models\TransactionType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {
            
            foreach (read_transaction_types_csv() as $item) {
                $journalType = JournalType::whereCode($item['journal_type'])->first();
                
                if (!$journalType) {
                    dd($item);
                }

                $item['journal_type_id'] = $journalType->getAttribute('id');

                $transactionType = TransactionType::updateOrCreate(
                    \Illuminate\Support\Arr::only($item, ['code']),
                    \Illuminate\Support\Arr::except($item, ['journal_type'])
                );
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
