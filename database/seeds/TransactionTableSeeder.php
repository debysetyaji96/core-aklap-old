<?php

use App\Models\JenisBukti;
use App\Models\JournalMode;
use App\Models\Transaction;
use Illuminate\Database\Seeder;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transaction::truncate();
        factory(Transaction::class, 500)->create();
    }
}
