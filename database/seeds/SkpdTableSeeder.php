<?php

use App\Models\SimdaSkpd;
use App\Models\Skpd;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SkpdTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        try {
            
            Skpd::truncate();

            $orderNumber = 1;

            DB::connection('pgsql_simda')->table('Referensi.r_skpd')
                ->select('id_skpd', 'kode_skpd', 'nama_skpd', 'id_daerah', 'tahun')
                ->where('is_skpd', 1)
                ->where('is_locked', 0)
                ->orderBy('id_skpd')
                ->each(function($data) use (&$orderNumber){
                        Skpd::create([
                            'id'=> $orderNumber,
                            'id_skpd_simda'=> $data->id_skpd,
                            'kode_skpd'=> $data->kode_skpd,
                            'nama_skpd'=> $data->nama_skpd,
                            'id_daerah'=> $data->id_daerah,
                            'tahun'=> $data->tahun,
                        ]);

                        ++$orderNumber;
                });

            // SimdaSkpd::query()->get()->each(function(SimdaSkpd $data) use (&$orderNumber){
            //     Skpd::create([
            //         'id'=> $orderNumber,
            //         'id_skpd_simda'=> $data->getAttribute('idSkpd'),
            //         'kode_skpd'=> $data->getAttribute('kodeSkpd'),
            //         'nama_skpd'=> $data->getAttribute('namaSkpd'),
            //         'id_daerah'=> $data->getAttribute('idDaerah'),
            //     ]);

            //     ++$orderNumber;
            // });

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
