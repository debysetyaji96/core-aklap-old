<?php

use App\Models\AccountBase;
use App\Models\AccountType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {

            $this->collection()->each(function ($item) {
                $accountType = AccountType::updateOrCreate(
                    \Illuminate\Support\Arr::only($item, ['code']),
                    \Illuminate\Support\Arr::except($item, ['account_base_code'])
                );
            });

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    protected function collection(): \Illuminate\Support\Collection
    {
        return collect([
            [
                'code'=> '0.0.00.00.00.0000',
                'name'=> 'Perubahan SAL dan Akun-akun penutup',
                'removable'=> false,
                'account_base_code'=> null,
            ],
            [
                'code'=> '1.0.00.00.00.0000',
                'name'=> 'Aset',
                'removable'=> false,
                'account_base_code'=> 'neraca',
            ],[
                'code'=> '2.0.00.00.00.0000',
                'name'=> 'Kewajiban',
                'removable'=> false,
                'account_base_code'=> 'neraca',
            ],[
                'code'=> '3.0.00.00.00.0000',
                'name'=> 'Ekuitas',
                'removable'=> false,
                'account_base_code'=> 'neraca',
            ],[
                'code'=> '4.0.00.00.00.0000',
                'name'=> 'Pendapatan-LRA',
                'removable'=> false,
                'account_base_code'=> 'lra',
            ],[
                'code'=> '5.0.00.00.00.0000',
                'name'=> 'Belanja',
                'removable'=> false,
                'account_base_code'=> 'lra',
            ],[
                'code'=> '6.0.00.00.00.0000',
                'name'=> 'Transfer',
                'removable'=> false,
                'account_base_code'=> 'lra',
            ],[
                'code'=> '7.0.00.00.00.0000',
                'name'=> 'Pembiayaan',
                'removable'=> false,
                'account_base_code'=> 'lra',
            ],[
                'code'=> '8.0.00.00.00.0000',
                'name'=> 'Pendapatan-LO',
                'removable'=> false,
                'account_base_code'=> 'lo',
            ],[
                'code'=> '9.0.00.00.00.0000',
                'name'=> 'Beban',
                'removable'=> false,
                'account_base_code'=> 'lo',
            ],
        ]);
    }
}
