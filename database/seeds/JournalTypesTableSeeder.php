<?php

use App\Models\JournalType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JournalTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {

            $this->collection()->each(function ($item) {
                $journalType = JournalType::updateOrCreate(
                    \Illuminate\Support\Arr::only($item, ['code']),
                    $item
                );
            });

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    protected function collection(): \Illuminate\Support\Collection
    {
        return collect([
            [
                'code'=> 'lo',
                'name'=> 'Laporan Operasional',
            ],[
                'code'=> 'lra',
                'name'=> 'Laporan Realisasi Anggaran',
            ],[
                'code'=> 'neraca',
                'name'=> 'Neraca',
            ],[
                'code'=> 'penutup',
                'name'=> 'Penutup',
            ],[
                'code'=> 'koreksi',
                'name'=> 'Koreksi',
            ],[
                'code'=> 'penyesuaian',
                'name'=> 'Penyesuaian',
            ],
        ]);
    }
}
