<?php

use App\Models\Journal;
use App\Models\JournalDetail;
use Illuminate\Database\Seeder;

class JournalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JournalDetail::truncate();
        Journal::truncate();
        factory(Journal::class, 50)->create();
    }
}
