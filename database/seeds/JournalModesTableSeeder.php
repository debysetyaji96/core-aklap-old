<?php

use App\Models\JournalMode;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JournalModesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {

            $this->collection()->each(function ($item) {
                $jenisBukti = JournalMode::updateOrCreate(
                    \Illuminate\Support\Arr::only($item, ['code']),
                    $item
                );
            });

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    protected function collection(): \Illuminate\Support\Collection
    {
        return collect([
            [
                'code'=> 'A',
                'name'=> 'Automatic',
            ],[
                'code'=> 'M',
                'name'=> 'Manual',
            ],[
                'code'=> 'R',
                'name'=> 'Revisi',
            ],
        ]);
    }
}
