<?php

use App\Models\JenisBukti;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenisBuktiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {

            $this->collection()->each(function ($item) {
                $jenisBukti = JenisBukti::updateOrCreate(
                    \Illuminate\Support\Arr::only($item, ['code']),
                    $item
                );
            });

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    protected function collection(): \Illuminate\Support\Collection
    {
        return collect([
            [
                'code'=> 'tbp',
                'name'=> 'TBP',
            ],[
                'code'=> 'skp',
                'name'=> 'SKP',
            ],[
                'code'=> 'skr',
                'name'=> 'SKR',
            ],
        ]);
    }
}
