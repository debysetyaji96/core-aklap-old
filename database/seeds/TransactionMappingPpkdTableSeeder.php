<?php

use App\Models\Account;
use App\Models\JournalType;
use App\Models\TransactionMapping;
use App\Models\TransactionType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionMappingPpkdTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {

            foreach (read_transaction_mapping_csv('ppkd') as $item) {
                $journalType = JournalType::whereCode($item['tipe_jurnal'])->first(); // lo, lra, neraca
                $transactionType = TransactionType::whereCode($item['kode_transaksi'])->first(); // anggaran-1, pendapatan-1
                $debitAccount = Account::whereCode($item['debit_kode_rekening'])->first();
                $creditAccount = Account::whereCode($item['credit_kode_rekening'])->first();

                if (!$transactionType) {
                    continue;
                }

                $mapping = TransactionMapping::create([
                    'mapping_type'=> 'skpd',
                    'journal_type_id'=> optional($journalType)->id,
                    'transaction_type_id'=> $transactionType->id,
                    'debit_category'=> $item['jenis_akun_debit'],
                    'debit_account_id'=> optional($debitAccount)->id,
                    'credit_category'=> $item['jenis_akun_credit'],
                    'credit_account_id'=> optional($creditAccount)->id,
                ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
